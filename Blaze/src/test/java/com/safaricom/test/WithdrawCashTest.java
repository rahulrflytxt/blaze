package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.mpesa.WithdrawcashHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class WithdrawCashTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public WithdrawcashHomePage withdrawObject=new WithdrawcashHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);

	
	@Test
	public void BLAZEWITHDRAWCASH_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.withdraCashClick)).isDisplayed();
		mpesahomeObject.withdraCashClick.click();
		String expectedtitle = "Withdraw Cash";
		wait.until(ExpectedConditions.elementToBeClickable(withdrawObject.withdrawcash_title)).isDisplayed();
		String withdrawcash_title = withdrawObject.withdrawcash_title.getText();
		System.out.println(withdrawcash_title);
		Assert.assertEquals(withdrawcash_title, expectedtitle);
		Assert.assertEquals(true, withdrawObject.edt_mobilenumber_WFAgent.isDisplayed(),"Enter mobile number field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawObject.et_amount_WFAgent.isDisplayed(),"Enter amount field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawObject.WFAgentContinueClick.isDisplayed(),"Continue button for WFAgent is not displayed");
		withdrawObject.l1_click.get(1).click();
		Thread.sleep(2000);
		Assert.assertEquals(true, withdrawObject.edt_agentnumber_WFATM.isDisplayed(),"Enter Mobile number field for WFATM is not displayed ");
		Assert.assertEquals(true, withdrawObject.WFATMContinueClick.isDisplayed(),"Continue button for WFATM is not displayed");


		

	}
	
	@Test
	public void BLAZEWITHDRAWCASH_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.withdraCashClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		
		withdrawObject.l1_click.get(0).click();
		withdrawObject.edt_mobilenumber_WFAgent
				.sendKeys("790771777");
		driver.navigate().back();
		withdrawObject.et_amount_WFAgent.sendKeys("200");
		driver.navigate().back();
		withdrawObject.WFAgentContinueClick.click();
		Thread.sleep(3000);
		String expected_confirmation_title ="Confirmation";
		wait.until(ExpectedConditions.elementToBeClickable(withdrawObject.confirmation_title)).isDisplayed();
		String actual_confirmation_title = withdrawObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = withdrawObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile value is Empty");
		String mpesa_agent_value = withdrawObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		String mpesa_amount_value = withdrawObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Send to value is empty");
		Assert.assertEquals(true, withdrawObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		withdrawObject.txt_continue_dilaog.click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.ok_btn.click();
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();
		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
		
		
	}
	
	@Test
	public void BLAZEWITHDRAWCASH_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.withdraCashClick.click();
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(withdrawObject.l1_click.get(1))).click();
		withdrawObject.edt_agentnumber_WFATM.sendKeys("1234");
		driver.navigate().back();
		withdrawObject.WFATMContinueClick.click();
		Thread.sleep(3000);
		String expected_confirmation_title = "Confirmation";
		wait.until(ExpectedConditions.elementToBeClickable( withdrawObject.confirmation_title)).isDisplayed();
		String actual_confirmation_title = withdrawObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String txt_business_val_atm = withdrawObject.txt_business_val_atm.getText();
		Assert.assertEquals(false, txt_business_val_atm.isEmpty(),"Businees value is empty");
		Assert.assertEquals(true, withdrawObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		withdrawObject.txt_continue_dilaog.click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.ok_btn.click();
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();
		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
	
		
	}
	
	
}
