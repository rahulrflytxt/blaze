package com.safaricom.test;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebonga.AccumulationHistoryPage;
import com.safaricom.blazebonga.BlazeBongaPage;
import com.safaricom.blazebonga.BlazebongaHomePage;
import com.safaricom.blazebonga.RedeemBongaPage;
import com.safaricom.blazebonga.RedemptionhistoryPage;
import com.safaricom.blazebonga.TransferbongapointsPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BlazeBongaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public BlazebongaHomePage blazebongaObject=new BlazebongaHomePage(driver);
	public BlazeBongaPage bongaobject=new BlazeBongaPage(driver);
	public TransferbongapointsPage transferbongaObject=new TransferbongapointsPage(driver);
	public RedeemBongaPage redeembongaObject=new RedeemBongaPage(driver);
	public RedemptionhistoryPage redemptionhistObject=new RedemptionhistoryPage(driver);
	public AccumulationHistoryPage accumulationhistoryObject=new AccumulationHistoryPage(driver);
	
	
	@Test
	public void BLAZEBONGA_TC_001() throws InterruptedException, IOException {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebongaObject.title)).isDisplayed();
		String expectedtitle="BLAZE BONGA";
		String actualtitle=blazebongaObject.title.getText();
		Assert.assertEquals(expectedtitle,actualtitle);
		List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
		Assert.assertEquals(true, linkElements.get(1).isDisplayed(),"Blaze Bonga is not displayed");
		Assert.assertEquals(true, blazebongaObject.seeredemptionhistory.isDisplayed(),"See Redemption History is not displayed");
		Assert.assertEquals(true, blazebongaObject.seeaccumulationhistory.isDisplayed(),"See Accumulation History is not displayed");
	}
	
	
	@Test
	public void BLAZEBONGA_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();

		}
		catch(Exception e)
		{
			System.out.println("Blaze Bonga Page");
		}
		List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
		wait.until(ExpectedConditions.elementToBeClickable(linkElements.get(1))).click();
		try {
			Thread.sleep(5000);
			bongaobject.certificateokbtn.click();
		}
		catch(Exception e)
		{
			System.out.println("No certificate popup");
		}
		Assert.assertEquals(true, bongaobject.myblazebongatab.isDisplayed(),"My Blaze Bonga tab is not displayed");
		Assert.assertEquals(true, bongaobject.bongaofferstab.isDisplayed(),"Bonga Offers tab is not displayed");
		Assert.assertEquals(true, bongaobject.date.isDisplayed(),"Date is not displayed");
		Assert.assertEquals(true, bongaobject.curbalance.isDisplayed(),"Balance is not displayed");
		Assert.assertEquals(true, bongaobject.transferbtn.isDisplayed(),"Transfer button is not displayed");
		Assert.assertEquals(true, bongaobject.redeembtn.isDisplayed(),"Redeem button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(bongaobject.bongaofferstab)).isDisplayed();
		bongaobject.bongaofferstab.click();
		Thread.sleep(2000);
		
	}
	
	@Test
	public void BLAZEBONGA_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();
			Thread.sleep(2000);
			List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
			wait.until(ExpectedConditions.elementToBeClickable(linkElements.get(1))).click();
			
		}
		catch(Exception e)
		{
			System.out.println("Blaze Bonga Page");
		}
		
		try {
			Thread.sleep(3000);
			bongaobject.certificateokbtn.click();
		}
		catch(Exception e)
		{
			System.out.println("No certificate popup");
		}
		wait.until(ExpectedConditions.elementToBeClickable(bongaobject.myblazebongatab)).isDisplayed();
		bongaobject.myblazebongatab.click();
		bongaobject.transferbtn.click();
		String expected="Bonga Services";
		String actual=transferbongaObject.title.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, transferbongaObject.numberfield.isDisplayed(),"Number field is not displayed");
		Assert.assertEquals(true, transferbongaObject.pointsfield.isDisplayed(),"Points field is not displayed");
		Assert.assertEquals(true, transferbongaObject.trasferbtn.isDisplayed(),"Transfer button is not displayed");
		transferbongaObject.numberfield.sendKeys("790771777");
		driver.navigate().back();
		transferbongaObject.pointsfield.sendKeys("10");
		driver.navigate().back();
		transferbongaObject.trasferbtn.click();
		String expected1="Please press OK to confirm a Bonga points transfer of 10 to 790771777.";
		String actual1=transferbongaObject.msg.getText();
		Assert.assertEquals(actual1, expected1);
		Thread.sleep(2000);
		transferbongaObject.okbtn1.click();
		Assert.assertEquals(true, transferbongaObject.servicetitle.isDisplayed(),"Service title is not displayed");
		Assert.assertEquals(true, transferbongaObject.pinfield.isDisplayed(),"Pin field is not displayed");
		Assert.assertEquals(true, transferbongaObject.serviceokbtn.isDisplayed(),"Ok button is not displayed");
		transferbongaObject.pinfield.sendKeys("1245");
		transferbongaObject.serviceokbtn.click();
		Thread.sleep(3000);
		String expected2="Dear Customer,The pin entered by you is wrong, Please try again.";
		String actual2=transferbongaObject.msg.getText();
		Assert.assertEquals(actual2, expected2);
		Thread.sleep(2000);
		transferbongaObject.okbtn1.click();
	}

	@Test
	public void BLAZEBONGA_TC_004() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();
			Thread.sleep(2500);
			List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
			wait.until(ExpectedConditions.elementToBeClickable(linkElements.get(1))).click();
			
		}
		catch(Exception e)
		{
			System.out.println("Blaze Bonga Page");
		}
		try {
			Thread.sleep(3000);
			bongaobject.certificateokbtn.click();
		}
		catch(Exception e)
		{
			System.out.println("No certificate popup");
		}
		wait.until(ExpectedConditions.elementToBeClickable(bongaobject.myblazebongatab)).isDisplayed();
		bongaobject.myblazebongatab.click();
		bongaobject.redeembtn.click();
		Thread.sleep(1000);
		String expected_maintitle = "Bonga Points";
		String actual_maintitle = redeembongaObject.title_bonga_points.getText();
		System.out.println(expected_maintitle + actual_maintitle);
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.redeem_option)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.redeem_option_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.amount_field)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.amount_field_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.redeem_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.ok_click)).click();
		Thread.sleep(1000);
		String expected_confirmationmessage = "Your request to redeem is successful. Thank you for staying with Safaricom.";
		String actual_confirmationmessage = redeembongaObject.final_confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.ok_click)).click();
		Thread.sleep(2000);
		driver.navigate().back();
	}

	@Test
	public void BLAZEBONGA_TC_005() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();
			
			
		}
		catch(Exception e)
		{
			System.out.println("Blaze Bonga Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(blazebongaObject.seeredemptionhistory)).isDisplayed();
		blazebongaObject.seeredemptionhistory.click();
		String expected_maintitle = "Redemption History";
		String actual_maintitle = redemptionhistObject.txt_dialog_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.text_date1)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.text_date2)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.tv_requestforbill)).click();
		Thread.sleep(3000);
		String expected_confirmationmessage = "Dear customer, there is no records available for your number.";
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.confirmation_message)).isDisplayed();

		String actual_confirmationmessage = redemptionhistObject.confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redemptionhistObject.ok_button1)).click();
		Thread.sleep(5000);
		
		
		
		
	}
	@Test
	public void BLAZEBONGA_TC_006() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebonga)).click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Blaze Bonga Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(blazebongaObject.seeaccumulationhistory)).isDisplayed();
		Thread.sleep(2000);
		blazebongaObject.seeaccumulationhistory.click();
		String expected_confirmationmessage = "Dear customer, there is no records available for your number.";
		wait.until(ExpectedConditions.elementToBeClickable(accumulationhistoryObject.confirmation_message)).isDisplayed();

		String actual_confirmationmessage = accumulationhistoryObject.confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(accumulationhistoryObject.ok_button)).click();
		Thread.sleep(2000);
		
		driver.navigate().back();
		
		
	}
}
