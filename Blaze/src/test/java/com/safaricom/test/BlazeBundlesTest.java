package com.safaricom.test;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebundles.BlazeBundleHomePage;
import com.safaricom.blazebundles.CheckBalanceHomePage;
import com.safaricom.blazebundles.CreateyouplanBuyAirtimePage;
import com.safaricom.blazebundles.CreateyourplanAirtimePage;
import com.safaricom.blazebundles.CreateyourplanFavoritePage;
import com.safaricom.blazebundles.CreateyourplanMyplansPage;
import com.safaricom.blazebundles.CreateyourplanMyprofilePage;
import com.safaricom.blazebundles.DailyHomePage;
import com.safaricom.blazebundles.DailyPopupPage;
import com.safaricom.blazebundles.DataCallingFinalConfirmPage;
import com.safaricom.blazebundles.DataCallingFirstPopupPage;
import com.safaricom.blazebundles.DataCallingPlanHomePage;
import com.safaricom.blazebundles.MyplanhistoryHomePage;
import com.safaricom.blazebundles.WeeklyHomePage;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BlazeBundlesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public BlazeBundleHomePage blazebundleObject=new BlazeBundleHomePage(driver);
	public HomePage homeObject=new HomePage(driver);
	public DataCallingPlanHomePage datacallObject=new DataCallingPlanHomePage(driver);
	public DataCallingFirstPopupPage dsplanActivateComfirmationPageObject=new  DataCallingFirstPopupPage(driver);
	public DataCallingFinalConfirmPage dsplanActivateFinalComfirmationPageObject = new DataCallingFinalConfirmPage(driver);
	public DailyHomePage dailyhomeObject=new DailyHomePage(driver);
	public DailyPopupPage dailypopupObject=new DailyPopupPage(driver);
	public WeeklyHomePage weeklyObject=new WeeklyHomePage(driver);
	public CreateyourplanMyprofilePage createhomeObject=new CreateyourplanMyprofilePage(driver);
	public CreateyourplanMyplansPage createmyplansObject=new CreateyourplanMyplansPage(driver);
	public CreateyourplanFavoritePage createfavoriteObject=new CreateyourplanFavoritePage(driver);
	public CreateyourplanAirtimePage createairtimeObject=new CreateyourplanAirtimePage(driver);
	public CreateyouplanBuyAirtimePage createbuyairtimeObject=new CreateyouplanBuyAirtimePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public CheckBalanceHomePage cheeckbalanceObject=new CheckBalanceHomePage(driver);
	public MyplanhistoryHomePage myplanObject=new MyplanhistoryHomePage(driver);
	
	
	

	@Test
	public void BLAZEBUNDLES_TC_001() throws InterruptedException, IOException {
	Thread.sleep(4000);
	wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.blazebundletitle)).isDisplayed();
	String expectedtitle="BLAZE BUNDLES";
	String actualtitle=blazebundleObject.blazebundletitle.getText();
	Assert.assertEquals(expectedtitle,actualtitle);
	Assert.assertEquals(true, blazebundleObject.datandcallingplans.isDisplayed(),"Data & Calling Plans option is not displayed");
	Assert.assertEquals(true, blazebundleObject.daily.isDisplayed(),"Daily option is not displayed");
	Assert.assertEquals(true, blazebundleObject.weekly.isDisplayed(),"Weekly Option is not displayed");
	Assert.assertEquals(true, blazebundleObject.monthly.isDisplayed(),"Monthly Option is not displayed");
	Assert.assertEquals(true, blazebundleObject.powerhour.isDisplayed(),"Power hour bundles option is not displayed");
	Assert.assertEquals(true, blazebundleObject.createyourplan.isDisplayed(),"Create your plan Option is not displayed");
	Assert.assertEquals(true, blazebundleObject.checkbalance.isDisplayed(),"Check balance Option is not displayed");
	Assert.assertEquals(true, blazebundleObject.myplanhistory.isDisplayed(),"My plan history Option is not displayed");

	}

	@Test
	public void BLAZEBUNDLES_TC_002() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		System.out.println("Blaze Bundles Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();
	String expectedtitledata="DATA & CALLING PLANS";
	String actualtitledata=datacallObject.datacallingtitle.getText();
	Assert.assertEquals(expectedtitledata,actualtitledata);
	Assert.assertEquals(true,datacallObject.airtimeBalance.isDisplayed(),"Airtime Balance is not displayed" );
	Assert.assertEquals(true,datacallObject.dataBundlesWithoutExpiry.isDisplayed(),"Data Bundles without Expiry is not displayed" );
	Assert.assertEquals(true,datacallObject.amountField.isDisplayed(),"Amount field is not displayed" );
	
	
	
	
	}

	@Test
	public void BLAZEBUNDLES_TC_003() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();

	}
	catch(Exception e)
	{
		System.out.println("Data & Calling Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).sendKeys("20");
	driver.navigate().back();
	List<MobileElement> linkElementsBuy = driver.findElements(By.id("com.selfcare.safaricom:id/txt_buy"));
	List<MobileElement> linkElements = driver.findElements(By.id("com.selfcare.safaricom:id/lin_lay"));

	Assert.assertEquals(true,linkElements.get(0).isDisplayed(),"No expiry Data plan is not displayed" );
	Assert.assertEquals(true,linkElementsBuy.get(0).isDisplayed(),"No expiry Data plan buy button is not displayed" );
	Assert.assertEquals(true,linkElements.get(1).isDisplayed(),"No expiry Call plan is not displayed" );
	Assert.assertEquals(true,linkElementsBuy.get(1).isDisplayed(),"No expiry Call plan buy button is not displayed" );
}

	@Test
	public void BLAZEBUNDLES_TC_004() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();

	}
	catch(Exception e)
	{
		System.out.println("Data & Calling Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).clear();
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).sendKeys("20");

	driver.navigate().back();
	List<MobileElement> linkElementsBuy = driver.findElements(By.id("com.selfcare.safaricom:id/txt_buy"));
	wait.until(ExpectedConditions.elementToBeClickable(linkElementsBuy.get(0))).click();
	Thread.sleep(2500);
	if(!(dsplanActivateComfirmationPageObject.ownnumber.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.ownnumber)).click();
	}
	if(!(dsplanActivateComfirmationPageObject.airtime.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.airtime)).click();
	}
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.continuebutton.isDisplayed(),"Continue button is not displayed");
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.cancelbutton.isDisplayed(),"Cancel button is not displayed");
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.continuebutton)).click();
	String expected="Confirm Purchase";
	String actual=dsplanActivateFinalComfirmationPageObject.confirmheader.getText();
	Assert.assertEquals(actual, expected);
	Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.confirmtxt.isDisplayed(),"Confirm message is not displayed");
	
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.buybtn)).click();
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.finalconfirmtext)).isDisplayed();
	String expectedfinalconfirm="Dear Customer, you have insufficient funds, kindly top up and try again or buy via M-PESA.";
	String expectedfinalconfirm2="Dear Customer,You have successfully purchased 70.18 MBs Data Bundle with no expiry.";
	String actualfinalconfirm=dsplanActivateFinalComfirmationPageObject.finalconfirmtext.getText();
	if(actualfinalconfirm.equals(expectedfinalconfirm))
	{
		Assert.assertEquals(actualfinalconfirm, expectedfinalconfirm);
	}

	else
	{
		Assert.assertEquals(actualfinalconfirm, expectedfinalconfirm2);
	}
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.okbtn)).click();

}

	@Test
	public void BLAZEBUNDLES_TC_005() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();

	}
	catch(Exception e)
	{
		System.out.println("Data & Calling Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).clear();
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).sendKeys("20");
	driver.navigate().back();
	List<MobileElement> linkElementsBuy = driver.findElements(By.id("com.selfcare.safaricom:id/txt_buy"));

	wait.until(ExpectedConditions.elementToBeClickable(linkElementsBuy.get(0))).click();
	Thread.sleep(2500);
	if(!(dsplanActivateComfirmationPageObject.othernumber.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.othernumber)).click();
	}
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.othernumbertext)).sendKeys("790837147");
	if(!(dsplanActivateComfirmationPageObject.airtime.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.airtime)).click();
	}
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.continuebutton.isDisplayed(),"Continue button is not displayed");
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.cancelbutton.isDisplayed(),"Cancel button is not displayed");
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.continuebutton)).click();
	String expected="Confirm Purchase";
	String actual=dsplanActivateFinalComfirmationPageObject.confirmheader.getText();
	Assert.assertEquals(actual, expected);
	Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.confirmtxt.isDisplayed(),"Confirm message is not displayed");
	
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.buybtn)).click();
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.finalconfirmtext)).isDisplayed();
	String expectedfinalconfirm="Dear Customer, you have insufficient funds, kindly top up and try again or buy via M-PESA.";
	String expectedfinalconfirm2="Dear Customer,You have successfully purchased 70.18 MBs Data Bundle for mobile number 0790837147 with no expiry.";
	String actualfinalconfirm=dsplanActivateFinalComfirmationPageObject.finalconfirmtext.getText();
	if(actualfinalconfirm.equals(expectedfinalconfirm))
	{
		Assert.assertEquals(actualfinalconfirm, expectedfinalconfirm);
	}

	else
	{
		Assert.assertEquals(actualfinalconfirm, expectedfinalconfirm2);

	}
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.okbtn)).click();


}
	
	@Test
	public void BLAZEBUNDLES_TC_006() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();

	}
	catch(Exception e)
	{
		System.out.println("Data & Calling Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).clear();
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).sendKeys("20");
	driver.navigate().back();
	List<MobileElement> linkElementsBuy = driver.findElements(By.id("com.selfcare.safaricom:id/txt_buy"));

	wait.until(ExpectedConditions.elementToBeClickable(linkElementsBuy.get(0))).click();
	Thread.sleep(2500);
	if(!(dsplanActivateComfirmationPageObject.ownnumber.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.ownnumber)).click();
	}
	if(!(dsplanActivateComfirmationPageObject.mpesa.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.mpesa)).click();
	}
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.continuebutton.isDisplayed(),"Continue button is not displayed");
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.cancelbutton.isDisplayed(),"Cancel button is not displayed");
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.continuebutton)).click();
	String expected="Please wait to enter M-PESA PIN";
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.finalmpesaconfirmtext)).isDisplayed();

	String actual=dsplanActivateFinalComfirmationPageObject.finalmpesaconfirmtext.getText();
	Assert.assertEquals(actual, expected);
	Thread.sleep(3000);
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.closebtn)).click();

}

	@Test
	public void BLAZEBUNDLES_TC_007() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.datandcallingplans)).click();

	}
	catch(Exception e)
	{
		System.out.println("Data & Calling Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).clear();
	wait.until(ExpectedConditions.elementToBeClickable(datacallObject.amountField)).sendKeys("20");
	driver.navigate().back();
	List<MobileElement> linkElementsBuy = driver.findElements(By.id("com.selfcare.safaricom:id/txt_buy"));

	wait.until(ExpectedConditions.elementToBeClickable(linkElementsBuy.get(0))).click();
	Thread.sleep(2500);
	if(!(dsplanActivateComfirmationPageObject.othernumber.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.othernumber)).click();
	}
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.othernumbertext)).sendKeys("790837147");
	if(!(dsplanActivateComfirmationPageObject.mpesa.isSelected()))
	{
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.mpesa)).click();
	}
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.continuebutton.isDisplayed(),"Continue button is not displayed");
	Assert.assertEquals(true, dsplanActivateComfirmationPageObject.cancelbutton.isDisplayed(),"Cancel button is not displayed");
	
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.continuebutton)).click();
	String expected="Please wait to enter M-PESA PIN";
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.finalmpesaconfirmtext)).isDisplayed();

	String actual=dsplanActivateFinalComfirmationPageObject.finalmpesaconfirmtext.getText();
	Assert.assertEquals(actual, expected);
	Thread.sleep(3000);
	wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.closebtn)).click();

}

	@Test
	public void BLAZEBUNDLES_TC_008() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		if(datacallObject.datacallingtitle.getText().equals("BLAZE BUNDLES PAGE"))
		{
			driver.navigate().back();
		}
		

	}
	wait.until(ExpectedConditions.elementToBeClickable(dailyhomeObject.dailytitle)).isDisplayed();

	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.daily)).click();
	List<MobileElement> linkElementsOffer = driver.findElements(By.id("com.selfcare.safaricom:id/offerTitle"));
	List<MobileElement> linkElementsPurchase = driver.findElements(By.id("com.selfcare.safaricom:id/buttonAction"));
	String expectedtitle="DAILY";
	wait.until(ExpectedConditions.elementToBeClickable(dailyhomeObject.dailytitle)).isDisplayed();

	String actualtitle=dailyhomeObject.dailytitle.getText();
	Assert.assertEquals(actualtitle, expectedtitle);
	Assert.assertEquals(true, linkElementsOffer.get(0).isDisplayed(),"Offer is not displayed");
	Assert.assertEquals(true, linkElementsPurchase.get(0).isDisplayed(),"Purchase button is displayed");
	
}
	@Test
	public void BLAZEBUNDLES_TC_009() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.daily)).click();

	}
	catch(Exception e)
	{
		System.out.println("Daily Page");
	}
	List<MobileElement> linkElementsPurchase = driver.findElements(By.id("com.selfcare.safaricom:id/buttonAction"));
	linkElementsPurchase.get(0).click();
	Thread.sleep(3000);
	Assert.assertEquals(true, dailypopupObject.buyoncebtn.isDisplayed(),"Buy Once button is not displayed");
	Assert.assertEquals(true, dailypopupObject.autorenewbtn.isDisplayed(),"Auto Renew button is not displayed");
	dailypopupObject.buyoncebtn.click();
	Thread.sleep(2500);
	Assert.assertEquals(true, dailypopupObject.confirmbuytitle.isDisplayed(),"Confirm buy title is not displayed");
	Assert.assertEquals(true, dailypopupObject.confirmmsg.isDisplayed(),"Confirm msg is not displayed");
	Assert.assertEquals(true, dailypopupObject.declinebtn.isDisplayed(),"Decline button is not displayed");
	Assert.assertEquals(true, dailypopupObject.acceptbtn.isDisplayed(),"Accept button is not displayed");
	dailypopupObject.acceptbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(dailypopupObject.successmsg)).isDisplayed();
	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=dailypopupObject.successmsg.getText();
	Assert.assertEquals(actual, expected);
	dailypopupObject.successokbtn.click();

	
}
	@Test
	public void BLAZEBUNDLES_TC_010() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.daily)).click();

	}
	catch(Exception e)
	{
		System.out.println("Daily Page");
	}
	List<MobileElement> linkElementsPurchase = driver.findElements(By.id("com.selfcare.safaricom:id/buttonAction"));
	linkElementsPurchase.get(0).click();
	Thread.sleep(3000);
	Assert.assertEquals(true, dailypopupObject.buyoncebtn.isDisplayed(),"Buy Once button is not displayed");
	Assert.assertEquals(true, dailypopupObject.autorenewbtn.isDisplayed(),"Auto Renew button is not displayed");
	dailypopupObject.autorenewbtn.click();
	Thread.sleep(2500);
	Assert.assertEquals(true, dailypopupObject.confirmbuytitle.isDisplayed(),"Confirm buy title is not displayed");
	Assert.assertEquals(true, dailypopupObject.confirmmsg.isDisplayed(),"Confirm msg is not displayed");
	Assert.assertEquals(true, dailypopupObject.declinebtn.isDisplayed(),"Decline button is not displayed");
	Assert.assertEquals(true, dailypopupObject.acceptbtn.isDisplayed(),"Accept button is not displayed");
	dailypopupObject.acceptbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(dailypopupObject.successmsg)).isDisplayed();
	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=dailypopupObject.successmsg.getText();
	Assert.assertEquals(actual, expected);
	dailypopupObject.successokbtn.click();
}

	@Test
	public void BLAZEBUNDLES_TC_011() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		if(dailyhomeObject.dailytitle.getText().equals("DAILY"))
		{
			driver.navigate().back();
		}
		

	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.blazebundletitle)).isDisplayed();
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.weekly)).click();
	List<MobileElement> linkElementsOffer = driver.findElements(By.id("com.selfcare.safaricom:id/offer_desc"));
	List<MobileElement> linkElementsPurchase = driver.findElements(By.id("com.selfcare.safaricom:id/radio_button"));
	String expectedtitle="BLAZE BUNDLES";
	String actualtitle=weeklyObject.weeklytitle.getText();
	Assert.assertEquals(actualtitle, expectedtitle);
	String expectedsubtitle="WEEKLY";
	String actualsubtitle=weeklyObject.subtitle.getText();
	Assert.assertEquals(actualsubtitle, expectedsubtitle);
	Assert.assertEquals(true, linkElementsOffer.get(0).isDisplayed(),"Offer is not displayed");
	Assert.assertEquals(true, linkElementsPurchase.get(0).isDisplayed(),"Purchase button is displayed");
	linkElementsPurchase.get(0).click();
	Assert.assertEquals(true, weeklyObject.confirmmsg.isDisplayed(),"Confirm message is not displayed");
	Assert.assertEquals(true, weeklyObject.okbtn.isDisplayed(),"Ok button is displayed");
	weeklyObject.okbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(weeklyObject.confirmmsg)).isDisplayed();
	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=weeklyObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	weeklyObject.okbtn.click();

	
	
}

	@Test
	public void BLAZEBUNDLES_TC_012() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		if(dailyhomeObject.dailytitle.getText().equals("DAILY"))
		{
			driver.navigate().back();
		}
		

	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.blazebundletitle)).isDisplayed();
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.monthly)).click();
	List<MobileElement> linkElementsOffer = driver.findElements(By.id("com.selfcare.safaricom:id/offer_desc"));
	List<MobileElement> linkElementsPurchase = driver.findElements(By.id("com.selfcare.safaricom:id/radio_button"));
	String expectedtitle="BLAZE BUNDLES";
	String actualtitle=weeklyObject.weeklytitle.getText();
	Assert.assertEquals(actualtitle, expectedtitle);
	String expectedsubtitle="MONTHLY";
	String actualsubtitle=weeklyObject.subtitle.getText();
	Assert.assertEquals(actualsubtitle, expectedsubtitle);
	Assert.assertEquals(true, linkElementsOffer.get(0).isDisplayed(),"Offer is not displayed");
	Assert.assertEquals(true, linkElementsPurchase.get(0).isDisplayed(),"Purchase button is displayed");
	linkElementsPurchase.get(0).click();
	Assert.assertEquals(true, weeklyObject.confirmmsg.isDisplayed(),"Confirm message is not displayed");
	Assert.assertEquals(true, weeklyObject.okbtn.isDisplayed(),"Ok button is displayed");
	weeklyObject.okbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(weeklyObject.confirmmsg)).isDisplayed();
	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=weeklyObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	weeklyObject.okbtn.click();

}

	@Test
	public void BLAZEBUNDLES_TC_013() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		if(dailyhomeObject.dailytitle.getText().equals("MONTHLY"))
		{
			driver.navigate().back();
		}
		

	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.blazebundletitle)).isDisplayed();
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.powerhour)).click();
	Thread.sleep(3000);
	driver.navigate().back();
	}
	public void BLAZEBUNDLES_TC_014() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		System.out.println("Blaze Bundles page");
		

	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.blazebundletitle)).isDisplayed();
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
	Assert.assertEquals(true, createhomeObject.myprofile.isDisplayed(),"My Profile tab is not displayed");
	Assert.assertEquals(true, createhomeObject.myplans.isDisplayed(),"My Plans tab is not displayed");
	Assert.assertEquals(true, createhomeObject.favouriteplans.isDisplayed(),"Favourite Plans tab is not displayed");
	
	createhomeObject.favouriteplans.click();
	Thread.sleep(4000);
	Assert.assertEquals(true, createhomeObject.airtime.isDisplayed(),"Airtime tab is not displayed");
	createhomeObject.airtime.click();
	Thread.sleep(2500);
	createhomeObject.myplans.click();
	Thread.sleep(2000);
	wait.until(ExpectedConditions.elementToBeClickable(createmyplansObject.msg)).isDisplayed();
	createmyplansObject.okbtn.click();
	
}

	@Test
	public void BLAZEBUNDLES_TC_015() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();

	}
	catch(Exception e)
	{
		System.out.println("Create your plan Page");
	}
	createhomeObject.myprofile.click();
	String expected="BLAZE BUNDLES";
	String actual=createhomeObject.title.getText();
	Assert.assertEquals(actual, expected);
	Assert.assertEquals(true, createhomeObject.name.isDisplayed(),"Name tab is not displayed");
	Assert.assertEquals(true, createhomeObject.airtimebalance.isDisplayed(),"Airtime balance is not displayed");
	Assert.assertEquals(true, createhomeObject.bundlebalance.isDisplayed(),"Bundle balance is not displayed");
	Assert.assertEquals(true, createhomeObject.dailybtn.isDisplayed(),"Daily tab is not displayed");
	Assert.assertEquals(true, createhomeObject.weeklybtn.isDisplayed(),"Weekly tab is not displayed");
	Assert.assertEquals(true, createhomeObject.monthlybtn.isDisplayed(),"Monthly tab is not displayed");
	createhomeObject.weeklybtn.click();
	Thread.sleep(3000);
	createhomeObject.monthlybtn.click();
	Thread.sleep(3000);
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
			+ "new UiSelector().text(\"IGNITE\"))");
	Assert.assertEquals(true, createhomeObject.dataoffer.isDisplayed(),"Data offer is not displayed");
	Assert.assertEquals(true, createhomeObject.calloffer.isDisplayed(),"Call offer is not displayed");
	Assert.assertEquals(true, createhomeObject.smsoffer.isDisplayed(),"Sms offer tab is not displayed");
	Assert.assertEquals(true, createhomeObject.ignitebtn.isDisplayed(),"Ignite button is not displayed");

}

	@Test
	public void BLAZEBUNDLES_TC_016() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();

	}
	catch(Exception e)
	{
		System.out.println("Create your plan Page");
	}
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
			+ "new UiSelector().text(\"IGNITE\"))");
	createhomeObject.ignitebtn.click();
	Assert.assertEquals(true, createhomeObject.confirmmsg.isDisplayed(),"Confirm message is not displayed");
	Assert.assertEquals(true, createhomeObject.okbtn.isDisplayed(),"Ok button is displayed");
	createhomeObject.okbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(createhomeObject.confirmmsg)).isDisplayed();
	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=createhomeObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	createhomeObject.okbtn.click();

}

	@Test
	public void BLAZEBUNDLES_TC_017() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();

	}
	catch(Exception e)
	{
		System.out.println("Create your plan Page");
	}
	createhomeObject.myplans.click();
	wait.until(ExpectedConditions.elementToBeClickable(createmyplansObject.msg)).isDisplayed();
	String expected="Dear customer, there is no plan available for your number.";
	String actual=createhomeObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	Thread.sleep(2000);
	createmyplansObject.okbtn.click();
	
	
}

	@Test
	public void BLAZEBUNDLES_TC_018() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();

	}
	catch(Exception e)
	{
		System.out.println("Create your plan Page");
	}
	createhomeObject.favouriteplans.click();
	Thread.sleep(3000);
	createfavoriteObject.dailydrop.click();
	Assert.assertEquals(true, createfavoriteObject.data.isDisplayed(),"Data is not displayed");
	Assert.assertEquals(true, createfavoriteObject.call.isDisplayed(),"Call is not displayed");
	Assert.assertEquals(true, createfavoriteObject.sms.isDisplayed(),"Sms is not displayed");
	Assert.assertEquals(true, createfavoriteObject.totalspend.isDisplayed(),"Total spend is not displayed");
	createfavoriteObject.applybtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(createfavoriteObject.confirmmsg)).isDisplayed();
	Assert.assertEquals(true, createfavoriteObject.confirmmsg.isDisplayed(),"Confirm message is not displayed");
	createfavoriteObject.okbtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(createfavoriteObject.confirmmsg)).isDisplayed();

	String expected="Dear Customer, your subscription is successful. Thank you for staying with Safaricom.";
	String actual=createfavoriteObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	createfavoriteObject.okbtn.click();
	
}

	@Test
	public void BLAZEBUNDLES_TC_019() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();

	}
	catch(Exception e)
	{
		System.out.println("Favourite plan Page");
	}
	createhomeObject.airtime.click();
	
	wait.until(ExpectedConditions.elementToBeClickable(createairtimeObject.myaitimebalancesubtitle)).isDisplayed();
	Assert.assertEquals(true, createairtimeObject.myaitimebalancesubtitle.isDisplayed(),"My airtime balance is not displayed");
	Assert.assertEquals(true, createairtimeObject.date.isDisplayed(),"Airtime date is not displayed");
	Assert.assertEquals(true, createairtimeObject.balance.isDisplayed(),"Balance is not displayed");
	Assert.assertEquals(true, createairtimeObject.topuptext.isDisplayed(),"Topup subtitle is not displayed");
	Assert.assertEquals(true, createairtimeObject.date2.isDisplayed(),"Topup date is not displayed");
	Assert.assertEquals(true, createairtimeObject.enter16text.isDisplayed(),"Enter pin text is not displayed");
	Assert.assertEquals(true, createairtimeObject.textfield16.isDisplayed(),"16 digit textfield is not displayed");
	Assert.assertEquals(true, createairtimeObject.okoajahazi.isDisplayed(),"Okoa Jahazi button is not displayed");
	Assert.assertEquals(true, createairtimeObject.loadairtime.isDisplayed(),"Load airtime button is not displayed");
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
			+ "new UiSelector().text(\"BUY AIRTIME WITH M-PESA\"))");
	Assert.assertEquals(true, createairtimeObject.buyairtimempesa.isDisplayed(),"Buy airtime with mpesa button is not displayed");

}

	@Test
	public void BLAZEBUNDLES_TC_020() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();

	}
	catch(Exception e)
	{
		System.out.println("Favorite plan Page");
	}
	createhomeObject.airtime.click();
	Thread.sleep(3000);
	createairtimeObject.okoajahazi.click();
	wait.until(ExpectedConditions.elementToBeClickable(createairtimeObject.title)).isDisplayed();
	String expected="OKOA Services";
	String actual=createairtimeObject.title.getText();
	Assert.assertEquals(actual, expected);
	Thread.sleep(2000);
	driver.navigate().back();
}

	@Test
	public void BLAZEBUNDLES_TC_021() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();

	}
	catch(Exception e)
	{
		System.out.println("Favorite plan Page");
	}
	createhomeObject.airtime.click();
	Thread.sleep(3000);
	createairtimeObject.textfield16.sendKeys("1234567890123456");
	driver.navigate().back();
	createairtimeObject.loadairtime.click();
	String expected="Dear Customer, The Voucher Card does not exist or is invalid. Thank you for staying with Safaricom.";
	String actual=createairtimeObject.confirmmsg.getText();
	Assert.assertEquals(actual, expected);
	createairtimeObject.okbtn.click();
}

	@Test
	public void BLAZEBUNDLES_TC_022() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();

	}
	catch(Exception e)
	{
		System.out.println("Favorite plan Page");
	}
	createhomeObject.airtime.click();
	Thread.sleep(3000);
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
			+ "new UiSelector().text(\"BUY AIRTIME WITH M-PESA\"))");
	createairtimeObject.buyairtimempesa.click();
	wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.title)).isDisplayed();
	String expected="Buy Airtime";
	String actual=createbuyairtimeObject.title.getText();
	Assert.assertEquals(actual, expected);
	
}

	@Test
	public void BLAZEBUNDLES_TC_023() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();
		createhomeObject.airtime.click();
		Thread.sleep(3000);
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
				+ "new UiSelector().text(\"BUY AIRTIME WITH M-PESA\"))");
		createairtimeObject.buyairtimempesa.click();
		wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.title)).isDisplayed();
	}
	catch(Exception e)
	{
		System.out.println("Buy Airtime Page");
	}
	
	createbuyairtimeObject.amount.sendKeys("20");
	createbuyairtimeObject.continuebtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.confirmtitle)).isDisplayed();
	Assert.assertEquals(true, createbuyairtimeObject.confirmtitle.isDisplayed(),"Confirm title is not displayed");
	Assert.assertEquals(true, createbuyairtimeObject.confirmamount.isDisplayed(),"Amount is not displayed");
	Assert.assertEquals(true, createbuyairtimeObject.sendbtn.isDisplayed(),"Send button is not displayed");
	createbuyairtimeObject.sendbtn.click();
	Thread.sleep(5000);
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).isDisplayed();

	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
	mpesapinObject.eight_btn.click();
	mpesapinObject.four_btn.click();
	mpesapinObject.four_btn.click();
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();

	String expected_label = "You have entered the wrong PIN!";
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

	String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
	Assert.assertEquals(actual_label, expected_label);
	System.out.println(actual_label);
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
	
	
}

	@Test
	public void BLAZEBUNDLES_TC_024() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.createyourplan)).click();
		createhomeObject.favouriteplans.click();
		createhomeObject.airtime.click();
		Thread.sleep(3000);
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
				+ "new UiSelector().text(\"BUY AIRTIME WITH M-PESA\"))");
		createairtimeObject.buyairtimempesa.click();
		wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.title)).isDisplayed();
	}
	catch(Exception e)
	{
		System.out.println("Buy Airtime Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.othernum)).click();
	createbuyairtimeObject.number.sendKeys("790771777");
	createbuyairtimeObject.amount.sendKeys("20");
	createbuyairtimeObject.continuebtn.click();
	wait.until(ExpectedConditions.elementToBeClickable(createbuyairtimeObject.confirmtitle)).isDisplayed();
	Assert.assertEquals(true, createbuyairtimeObject.confirmtitle.isDisplayed(),"Confirm title is not displayed");
	Assert.assertEquals(true, createbuyairtimeObject.confirmamount.isDisplayed(),"Amount is not displayed");
	Assert.assertEquals(true, createbuyairtimeObject.sendbtn.isDisplayed(),"Send button is not displayed");
	createbuyairtimeObject.sendbtn.click();
	Thread.sleep(5000);
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).isDisplayed();

	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
	mpesapinObject.eight_btn.click();
	mpesapinObject.four_btn.click();
	mpesapinObject.four_btn.click();
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();

	String expected_label = "You have entered the wrong PIN!";
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

	String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
	Assert.assertEquals(actual_label, expected_label);
	System.out.println(actual_label);
	wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
	
	
}

	@Test
	public void BLAZEBUNDLES_TC_025() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(6000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		System.out.println("Blaze Bundles Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.checkbalance)).click();
	String expected="My Account";
	String actual=createhomeObject.title.getText();
	Assert.assertEquals(actual, expected);
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.balance_tab_click)).click();
	Assert.assertEquals(true, cheeckbalanceObject.balance_tab_click.isSelected(),"Balance tab is selected");
	Assert.assertEquals(true, cheeckbalanceObject.data_balance_view.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.bonga_balance_view.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.airtime_balance_view.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.sms_balance_view.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.okoa_balance_view.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.voice_balance_view.isDisplayed());
	
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.data_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.limited_data_amount.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.daily_data_amount.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.data_balance_view)).click();
	
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.bonga_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.bonga_balance.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.bonga_balance_view)).click();
	
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.airtime_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.airtime_prepaid_balance.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.airtime_balance_view)).click();
	
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.sms_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.daily_sms_balance.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.sms_balance_view)).click();
	
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.okoa_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.okoa_airtime_balance.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.okoa_data_balance.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.okoa_balance_view)).click();
			
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.voice_balance_view)).click();
	Assert.assertEquals(true, cheeckbalanceObject.voice_on_net_talktime.isDisplayed());
	Assert.assertEquals(true, cheeckbalanceObject.voice_off_net_talktime.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(cheeckbalanceObject.voice_balance_view)).click();
Thread.sleep(3000);
driver.navigate().back();
	
}

	@Test
	public void BLAZEBUNDLES_TC_026() throws InterruptedException, IOException {
	try
	{
		Thread.sleep(4000);
		homeObject.exploremsg.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazebundles)).click();

	}
	catch(Exception e)
	{
		System.out.println("Blaze Bundles Page");
	}
	wait.until(ExpectedConditions.elementToBeClickable(blazebundleObject.myplanhistory)).click();
	String expected="BLAZE BUNDLES";
	String actual=createhomeObject.title.getText();
	Assert.assertEquals(actual, expected);
	Assert.assertEquals(true, myplanObject.fromdate.isDisplayed(),"From date not displayed");
	Assert.assertEquals(true, myplanObject.todate.isDisplayed(),"To date not displayed");
	Assert.assertEquals(true, myplanObject.generatebtn.isDisplayed(),"Generate button is not displayed");
	myplanObject.fromdate.click();
	myplanObject.prevbtn.click();
	myplanObject.prevbtn.click();
	driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.view.View\")).scrollIntoView("
			+ "new UiSelector().text(\"1\"))").click();
	myplanObject.okbtn.click();
	myplanObject.todate.click();
	myplanObject.okbtn.click();
	myplanObject.generatebtn.click();
	String expected1="Dear customer, there is no records available for your number.";
	String actual1=myplanObject.msg.getText();
	Assert.assertEquals(actual1, expected1);
	Thread.sleep(2000);
	myplanObject.okbtn1.click();
	driver.navigate().back();
	Thread.sleep(2000);
	driver.navigate().back();
}





}
