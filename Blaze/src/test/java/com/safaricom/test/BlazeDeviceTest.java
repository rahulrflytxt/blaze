package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazedevice.BlazedeviceHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import junit.framework.Assert;

public class BlazeDeviceTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public BlazedeviceHomePage blazedeviceObject=new BlazedeviceHomePage(driver);
	
	@Test
	public void BLAZEDEVICES_TC_001() throws InterruptedException, IOException {
		try
		{
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();
			}
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazedevices)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazedeviceObject.blazedevice_title)).isDisplayed();
		String expected="BLAZE DEVICES";
		String actual=blazedeviceObject.blazedevice_title.getText();
		Assert.assertEquals(expected, actual);
		Thread.sleep(3000);
	}
}
