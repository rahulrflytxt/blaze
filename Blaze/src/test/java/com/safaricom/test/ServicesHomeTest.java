package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.talktous.FaqsHomePage;
import com.safaricom.talktous.LivechatHomepage;
import com.safaricom.talktous.ShareyourstoryPage;
import com.safaricom.talktous.TalktousHomepage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	
	@Test
	public void SERVICES_TC_001() throws InterruptedException, IOException {
	try
	{
		homeObject.exploremsg.isDisplayed();
		
	}
	catch(Exception e)
	{
		driver.navigate().back();
	}
	Thread.sleep(4000);
	wait.until(ExpectedConditions.elementToBeClickable(menuObject.menu_click)).click();
	MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
			+ "new UiSelector().text(\"Services\"))");
	service.click();
	Thread.sleep(2500);
	String expected_confirmation_title = "Services";
	wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();

	String actual_confirmation_title = servicesObject.services_title.getText();
	Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
	Assert.assertEquals(true, servicesObject.checkbalance_click.isDisplayed(),"Check Balance icon is not displayed");
	Assert.assertEquals(true, servicesObject.bongaservices_click.isDisplayed(),"Bonga Services is not displayed");
	Assert.assertEquals(true, servicesObject.airtimetopup_click.isDisplayed(),"Airtime Topup icon is not displayed");
	Assert.assertEquals(true, servicesObject.datacall_click.isDisplayed(),"Data Calling Plan icon is not displayed");
	Assert.assertEquals(true, servicesObject.okoa_click.isDisplayed(),"Okoa Services icon is not displayed");
	Assert.assertEquals(true, servicesObject.sambaza_click.isDisplayed(),"Sambaza Services icon is not displayed");
	Assert.assertEquals(true, servicesObject.storelocator_click.isDisplayed(),"Store Locator icon is not displayed");
	Assert.assertEquals(true, servicesObject.contactus_click.isDisplayed(),"Contact Us icon is not displayed");
}
}
