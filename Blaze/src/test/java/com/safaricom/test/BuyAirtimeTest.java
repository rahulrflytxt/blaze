package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.BuyAirtimeHomePage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.mpesa.WithdrawcashHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BuyAirtimeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public BuyAirtimeHomePage buyairtimeObject=new BuyAirtimeHomePage(driver);
	
	@Test
	public void BLAZEBUYAIRTIME_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.buyairtimeClick)).isDisplayed();
		mpesahomeObject.buyairtimeClick.click();
		String expectedtitle = "Buy Airtime";
		String buyAirtime_title = buyairtimeObject.buyAirtime_title.getText();
		System.out.println(buyAirtime_title);
		Assert.assertEquals(buyAirtime_title, expectedtitle);
		buyairtimeObject.selfClick.click();
		Assert.assertEquals(true, buyairtimeObject.self_amount.isDisplayed(),"Enter Pin is displayed");
		Assert.assertEquals(true, buyairtimeObject.self_continue.isDisplayed(),"Continue button is displayed");
		buyairtimeObject.otherClick.click();
		Assert.assertEquals(true, buyairtimeObject.inputPhoneNumber.isDisplayed(),"Mobile number field is displayed");
		Assert.assertEquals(true, buyairtimeObject.inputAmount.isDisplayed(),"Enter pin for Other number is displayed");
		Assert.assertEquals(true, buyairtimeObject.other_continue.isDisplayed(),"Continue button is displayed");
		
	}
	
	@Test
	public void BLAZEBUYAIRTIME_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.buyairtimeClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		buyairtimeObject.selfClick.click();
		buyairtimeObject.self_amount.sendKeys("1234");
		driver.navigate().back();
		buyairtimeObject.self_continue.click();
		Thread.sleep(3000);
		String expected_confirmation_title = "Confirmation";
		String actual_confirmation_title = buyairtimeObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_agent_value = buyairtimeObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent Value is empty");
		String mpesa_amount_value = buyairtimeObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount Value is empty");
		Assert.assertEquals(true, buyairtimeObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, buyairtimeObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		buyairtimeObject.txt_continue_dilaog
				.click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();
	
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
		

		
	}
	
	@Test
	public void BLAZEBUYAIRTIME_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.buyairtimeClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(buyairtimeObject.otherClick)).click();
		buyairtimeObject.inputPhoneNumber.sendKeys("790771777");
		driver.navigate().back();
		buyairtimeObject.inputAmount.sendKeys("1234");
		driver.navigate().back();
		buyairtimeObject.other_continue.click();

		Thread.sleep(4000);
		String expected_confirmation_title = "Confirmation";
		
		String actual_confirmation_title = buyairtimeObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = buyairtimeObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile value is empty");
		String mpesa_agent_value = buyairtimeObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		String mpesa_amount_value = buyairtimeObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount is empty");
		Assert.assertEquals(true, buyairtimeObject.txt_continue_dilaog.isEnabled(),"Continue  button is disabled");
		Assert.assertEquals(true, buyairtimeObject.txt_cancel_dilaog.isEnabled());
		buyairtimeObject.txt_continue_dilaog
				.click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();
	
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
		
	}
	
}
