package com.safaricom.test;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.AboutusPage;
import com.safaricom.menu.BacktoSafaricomPage;
import com.safaricom.menu.BlazeblogPage;
import com.safaricom.menu.ChangeservicepinPage;
import com.safaricom.menu.FeedbackPage;
import com.safaricom.menu.LogoutPage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.menu.MyAccountPUKPage;
import com.safaricom.menu.MyAccountPage;
import com.safaricom.menu.MyAccountTopupPage;
import com.safaricom.menu.MyProfilePage;
import com.safaricom.menu.OffersPage;
import com.safaricom.menu.SecurewithpinPage;
import com.safaricom.menu.SettingsPage;
import com.safaricom.menu.TellafriendPage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesSkizaPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MenuTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public MyProfilePage profileObject=new MyProfilePage(driver);
	public MyAccountPage myaccountObject=new MyAccountPage(driver);
	public MyAccountPUKPage pukObject=new MyAccountPUKPage(driver);
	public MyAccountTopupPage topupObject=new MyAccountTopupPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public BlazeblogPage blazeblogObject=new BlazeblogPage(driver);
	public OffersPage offersObject=new OffersPage(driver);
	public SettingsPage settingsObject=new SettingsPage(driver);
	public ChangeservicepinPage changepinObject=new ChangeservicepinPage(driver);
	public SecurewithpinPage secureObject=new SecurewithpinPage(driver);
	public TellafriendPage tellObject=new TellafriendPage(driver);
	public FeedbackPage feedbackObject=new FeedbackPage(driver);
	public AboutusPage aboutObject=new AboutusPage(driver);
	public BacktoSafaricomPage backtoObject=new BacktoSafaricomPage(driver);
	public LogoutPage logoutObject=new LogoutPage(driver);
	
	@Test
	public void MENU_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement home = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Home\"))");
		home.click();
		Thread.sleep(2500);	
		Assert.assertEquals(true, homeObject.welcomemsg.isDisplayed(),"Welcome message is not displayed");
		Assert.assertEquals(true, homeObject.name.isDisplayed(),"Name is not displayed");
		String expected="Explore the BLAZE world";
		String actual=homeObject.exploremsg.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, homeObject.beyourboss.isDisplayed(),"Be your boss icon is not displayed");
		Assert.assertEquals(true, homeObject.blazebundles.isDisplayed(),"Blaze Bundles icon is not displayed");
		Assert.assertEquals(true, homeObject.blazedatausage.isDisplayed(),"Blaze data usage icon is not displayed");
		Assert.assertEquals(true, homeObject.blazebonga.isDisplayed(),"Blaze Bonga icon is not displayed");	
		Assert.assertEquals(true, homeObject.blazempesa.isDisplayed(),"Blaze Mpesa icon is not displayed");
		Assert.assertEquals(true, homeObject.blazedevices.isDisplayed(),"Blaze Devices icon is not displayed");
		Assert.assertEquals(true, homeObject.talktous.isDisplayed(),"Talk to us icon is not displayed");	
		
	}
	@Test
	public void MENU_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement myprofile = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"My Profile\"))");
		myprofile.click();
		Thread.sleep(2500);	
		String expected="My Profile";
		String actual=profileObject.myprofile_title.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, profileObject.profilepic.isDisplayed(),"Profile pic is not displayed");
		Assert.assertEquals(true, profileObject.name.isDisplayed(),"Profile name is not displayed");
		Assert.assertEquals(true, profileObject.email.isDisplayed(),"Email is not displayed");
		Assert.assertEquals(true, profileObject.emailtxt.isDisplayed(),"Email txtfield is not displayed");
		Assert.assertEquals(true, profileObject.imei.isDisplayed(),"Imei is not displayed");
		Assert.assertEquals(true, profileObject.imeitxt.isDisplayed(),"Imei txtfield is not displayed");
		Assert.assertEquals(true, profileObject.puk.isDisplayed(),"Puk is not displayed");
		Assert.assertEquals(true, profileObject.puktxt.isDisplayed(),"Puk txtfield is not displayed");
		Thread.sleep(2500);
		driver.navigate().back();
	}
	@Test
	public void MENU_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement myaccount = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"My Account\"))");
		myaccount.click();
		Thread.sleep(2500);	
		String expected="My Account";
		String actual=myaccountObject.txt_title.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.balance_tab_click)).click();
		Assert.assertEquals(true, myaccountObject.balance_tab_click.isSelected(),"Balance tab is selected");
		
		Assert.assertEquals(true, myaccountObject.data_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccountObject.bonga_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccountObject.airtime_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccountObject.sms_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccountObject.okoa_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccountObject.voice_balance_view.isDisplayed());
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.data_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.limited_data_amount.isDisplayed());
		Assert.assertEquals(true, myaccountObject.daily_data_amount.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.data_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.bonga_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.bonga_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.bonga_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.airtime_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.airtime_prepaid_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.airtime_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.sms_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.daily_sms_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.sms_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.okoa_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.okoa_airtime_balance.isDisplayed());
		Assert.assertEquals(true, myaccountObject.okoa_data_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.okoa_balance_view)).click();
				
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.voice_balance_view)).click();
		Assert.assertEquals(true, myaccountObject.voice_on_net_talktime.isDisplayed());
		Assert.assertEquals(true, myaccountObject.voice_off_net_talktime.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.voice_balance_view)).click();
		
		
		
	}
	@Test
	public void MENU_TC_004() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement myaccount = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"My Account\"))");
			myaccount.click();
			Thread.sleep(2500);	
		}
		catch(Exception e)
		{
			System.out.println("My Account Page");		
			}
		myaccountObject.puk_tab_click.click();
		pukObject.mynumber.click();
		Assert.assertEquals(true, pukObject.field.isDisplayed());
		Assert.assertEquals(true, pukObject.copy.isDisplayed());
		pukObject.copy.click();
		Assert.assertEquals(true, pukObject.contactus.isDisplayed());
		Thread.sleep(2000);
		pukObject.othernumber.click();
		Assert.assertEquals(true, pukObject.othernumber.isDisplayed());
		Assert.assertEquals(true, pukObject.mynumber.isDisplayed());
		Assert.assertEquals(true, pukObject.mobnumfield.isDisplayed());
		Assert.assertEquals(true, pukObject.idfield.isDisplayed());
		Assert.assertEquals(true, pukObject.sendbtn.isDisplayed());
		Assert.assertEquals(true, pukObject.contactus.isDisplayed());
		pukObject.mobnumfield.sendKeys("790771777");
		pukObject.idfield.sendKeys("12345678");
		pukObject.sendbtn.click();
		String expected="Please enter your ID Number used during registration. Thank you.";
		wait.until(ExpectedConditions.elementToBeClickable(pukObject.msg)).isDisplayed();
		String actual=pukObject.msg.getText();
		Assert.assertEquals(actual, expected);
		pukObject.okbtn.click();
		pukObject.contactus.click();
		String expectedc="Contact Us";
		String actualc =pukObject.txt_title.getText();
		Assert.assertEquals(actualc, expectedc);
		driver.navigate().back();
	}
	@Test
	public void MENU_TC_005() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement myaccount = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"My Account\"))");
			myaccount.click();
			Thread.sleep(2500);	
		}
		catch(Exception e)
		{
			System.out.println("My Account Page");		
		}
		myaccountObject.topup_tab_click.click();
		Assert.assertEquals(true, topupObject.date.isDisplayed());
		Assert.assertEquals(true, topupObject.balance.isDisplayed());
		Assert.assertEquals(true, topupObject.mynumber.isDisplayed());
		Assert.assertEquals(true, topupObject.othernumber.isDisplayed());
		Assert.assertEquals(true, topupObject.digitfield.isDisplayed());
		Assert.assertEquals(true, topupObject.sendbtn.isDisplayed());
		Assert.assertEquals(true, topupObject.mpesatopupbtn.isDisplayed());
		Assert.assertEquals(true, topupObject.redeembongabtn.isDisplayed());
		Assert.assertEquals(true, topupObject.contactus.isDisplayed());
		topupObject.digitfield.sendKeys("1234567890123456");
		topupObject.sendbtn.click();
		String expected="Dear Customer, The Voucher Card does not exist or is invalid. Thank you for staying with Safaricom.";
		wait.until(ExpectedConditions.elementToBeClickable(topupObject.msg)).isDisplayed();
		String actual=topupObject.msg.getText();
		Assert.assertEquals(actual, expected);
		topupObject.okbtn.click();
		topupObject.othernumber.click();
		Assert.assertEquals(true, topupObject.date.isDisplayed());
		Assert.assertEquals(true, topupObject.balance.isDisplayed());
		Assert.assertEquals(true, topupObject.numberfield.isDisplayed());
		Assert.assertEquals(true, topupObject.digitfield.isDisplayed());
		Assert.assertEquals(true, topupObject.sendbtn.isDisplayed());
		Assert.assertEquals(true, topupObject.mpesatopupbtn.isDisplayed());
		Assert.assertEquals(true, topupObject.redeembongabtn.isDisplayed());
		Assert.assertEquals(true, topupObject.contactus.isDisplayed());
		topupObject.numberfield.sendKeys("790771777");
		topupObject.digitfield.sendKeys("1234567890123456");
		topupObject.sendbtn.click();
		String expected1="Dear Customer, The Voucher Card does not exist or is invalid. Thank you for staying with Safaricom.";
		wait.until(ExpectedConditions.elementToBeClickable(topupObject.msg)).isDisplayed();
		String actual1=topupObject.msg.getText();
		Assert.assertEquals(actual1, expected1);
		topupObject.okbtn.click();
		topupObject.mpesatopupbtn.click();
		String expected2="Buy Airtime";
		String actual2=topupObject.txt_title.getText();
		Assert.assertEquals(actual2, expected2);
		Thread.sleep(2000);
		driver.navigate().back();
		topupObject.redeembongabtn.click();
		String expected3="Bonga Services";
		String actual3=topupObject.txt_title.getText();
		Assert.assertEquals(actual3, expected3);
		Thread.sleep(2000);
		driver.navigate().back();
	
	}
	@Test
	public void MENU_TC_006() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement services = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		services.click();
		Thread.sleep(2500);	
		String expected_confirmation_title = "Services";
		wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();

		String actual_confirmation_title = servicesObject.services_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		Assert.assertEquals(true, servicesObject.checkbalance_click.isDisplayed(),"Check Balance icon is not displayed");
		Assert.assertEquals(true, servicesObject.bongaservices_click.isDisplayed(),"Bonga Services is not displayed");
		Assert.assertEquals(true, servicesObject.airtimetopup_click.isDisplayed(),"Airtime Topup icon is not displayed");
		Assert.assertEquals(true, servicesObject.datacall_click.isDisplayed(),"Data Calling Plan icon is not displayed");
		Assert.assertEquals(true, servicesObject.okoa_click.isDisplayed(),"Okoa Services icon is not displayed");
		Assert.assertEquals(true, servicesObject.sambaza_click.isDisplayed(),"Sambaza Services icon is not displayed");
		Assert.assertEquals(true, servicesObject.storelocator_click.isDisplayed(),"Store Locator icon is not displayed");
		Assert.assertEquals(true, servicesObject.contactus_click.isDisplayed(),"Contact Us icon is not displayed");
		
	}
	@Test
	public void MENU_TC_007() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement mpesa = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"M-PESA\"))");
		mpesa.click();
		Thread.sleep(2500);	
		String title = "M-PESA";
		String sfctitle = mpesahomeObject.mpesa_title.getText();
		Assert.assertEquals(true, mpesahomeObject.mpesa_show_balance.isDisplayed());
		Assert.assertEquals(sfctitle, title);
		Assert.assertEquals(true, mpesahomeObject.sendMoneyClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.withdraCashClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.buyairtimeClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.lipanampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.billmanagerClick.isDisplayed());

		Assert.assertEquals(true, mpesahomeObject.loansandsavingsClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.myaccountClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.fulizampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.mpesaglobalClick.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"Scan QR\"))");	
		Assert.assertEquals(true, mpesahomeObject.mpesa_buttonScanQR.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"M-PESA Statement\"))");	
		Assert.assertEquals(true, mpesahomeObject.mpesa_statement.isDisplayed());
		
		
	}
	@Test
	public void MENU_TC_008() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement offers = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Offers\"))");
		offers.click();
		Assert.assertEquals(true, offersObject.offers_title.isDisplayed());
		String expected="OFFERS";
		String actual=offersObject.offers_title.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, offersObject.recentsubtitle.isDisplayed());
		List<MobileElement> Offers = driver.findElementsByClassName("android.widget.LinearLayout");
		Assert.assertEquals(true, Offers.get(0).isDisplayed());
		List<MobileElement> ignitebtn = driver.findElementsByXPath("//android.widget.Button[@text='IGNITE']");
		ignitebtn.get(0).click();;
		Assert.assertEquals(true, offersObject.cofirmmsg.isDisplayed());
		Assert.assertEquals(true, offersObject.acceptbtn.isDisplayed());
		Assert.assertEquals(true, offersObject.declinebtn.isDisplayed());
		offersObject.acceptbtn.click();
		String expected6="Kindly wait as we process your request. Thank you";
		String expected7="Please wait for one minute before attempting to purchase another offer...";
		String actual6=offersObject.msg.getText();
		if(expected6.equals(actual6))
		{
		Assert.assertEquals(actual6, expected6);
		}
		else
		{
			Assert.assertEquals(actual6, expected7);
		
		}
		offersObject.okbtn.click();
	}
	@Test
	public void MENU_TC_009() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement blazeblog = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"BLAZE Blog\"))");
		blazeblog.click();
		Thread.sleep(2500);	
		String expectedtitle = "BLAZE BLOG";
		String actualtitle=blazeblogObject.title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		
	}
	@Test
	public void MENU_TC_010() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Settings\"))");
		settings.click();
		Thread.sleep(2500);	
		Assert.assertEquals(true, settingsObject.changeservicepin_click.isDisplayed(),
				"Change Service PIN is not displayed");
		Assert.assertEquals(true, settingsObject.securewithpin_clicks.isDisplayed(),
				"Secure With PIN is ot displayed");
		
	}
	@Test
	public void MENU_TC_011() throws InterruptedException, IOException {
		
		
		
	}
	@Test
	public void MENU_TC_012() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Settings\"))");
			settings.click();
		}
		catch(Exception e)
		{
			System.out.println("Settings");	
		}
		settingsObject.changeservicepin_click.click();
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.et_old_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.et_national_id)).sendKeys("1234567890");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.et_new_pin)).sendKeys("1221");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.et_confirm_pin)).sendKeys("1221");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.tv_submit)).click();
		String expected="Please enter the correct National ID.";
		String actual=changepinObject.tv_message.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(changepinObject.tv_ok)).click();
		Thread.sleep(2000);
		driver.navigate().back();
		
		
	}
	@Test
	public void MENU_TC_013() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			System.out.println("Settings");	
		}
		menuObject.menu_click.click();
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Settings\"))");
		settings.click();
		settingsObject.securewithpin_clicks.click();
		wait.until(ExpectedConditions.elementToBeClickable(secureObject.confirmtxt)).isDisplayed();

		String actual=secureObject.confirmtxt.getText();
		String expected="Are you sure you want to enable Secure with PIN feature for your account?";
		Assert.assertEquals(actual, expected);
		secureObject.confirmyesbtn.click();
		String expectedheader="Enter the Service PIN";
		wait.until(ExpectedConditions.elementToBeClickable(secureObject.header)).isDisplayed();

		String actualheader=secureObject.header.getText();
		Assert.assertEquals(actualheader, expectedheader);
		secureObject.pinfield.sendKeys("7890");
		secureObject.okbtn.click();
		wait.until(ExpectedConditions.elementToBeClickable(secureObject.confirmtxt)).isDisplayed();
		String expectedmsg="Provided PIN is invalid, Please try again with a valid Service PIN.";
		String actualmsg=secureObject.confirmtxt.getText();
		Assert.assertEquals(actualmsg, expectedmsg);
		secureObject.confirmyesbtn.click();
	}
	@Test
	public void MENU_TC_014() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement tellfriend = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Tell a friend\"))");
		tellfriend.click();
		Thread.sleep(2500);
		String expected_maintitle = "Tell a friend";
		String actual_maintitle = tellObject.tellafriendtitle.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Thread.sleep(2500);
		driver.navigate().back();
	}
	@Test
	public void MENU_TC_015() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement feedback = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Feedback & Rating\"))");
		feedback.click();
		Thread.sleep(2500);
		String expected_maintitle = "Feedback & Rating";
		String actual_maintitle = feedbackObject.feedback_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_category_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_Category_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_email)).sendKeys("rahul.r@flytxt.com");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_subject)).sendKeys("Trasaction");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_msg)).sendKeys("Trasaction submit");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.feedback_submit)).click();
		Thread.sleep(3000);
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(feedbackObject.ok_btn)).click();
		driver.navigate().back();
		}
	@Test
	public void MENU_TC_016() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement about = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"About the App\"))");
		about.click();
		Thread.sleep(2500);
		String expected_maintitle = "About app";
		
		String actual_maintitle = aboutObject.aboutUs_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, aboutObject.app_name.isDisplayed(),"App name is not displayed");
		Assert.assertEquals(true, aboutObject.app_version.isDisplayed(), "App version is not displayed");
		Assert.assertEquals(true, aboutObject.terms_and_condn.isDisplayed(), "Terms and Condition is not displayed");
		Assert.assertEquals(true, aboutObject.firstlogin.isDisplayed(), "First login is not displayed");
		Assert.assertEquals(true, aboutObject.lastaccess.isDisplayed(), "Last access is not displayed");
		Thread.sleep(2500);
		driver.navigate().back();
	}
	@Test
	public void MENU_TC_017() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement backto = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Back to MySafaricom\"))");
		backto.click();
		wait.until(ExpectedConditions.elementToBeClickable(backtoObject.tc_msg)).isDisplayed();
		backtoObject.tc_ok.click();
		wait.until(ExpectedConditions.elementToBeClickable(backtoObject.success_msg)).isDisplayed();
		backtoObject.success_ok.click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.menutab_click)).click();
		MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
				+ "new UiSelector().text(\"Back to Blaze\"))");
		elementToClick.click();
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.ok_button)).click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.okbutton)).click();
	}
	@Test
	public void MENU_TC_018() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement logout = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Log Out\"))");
		logout.click();
		wait.until(ExpectedConditions.elementToBeClickable(logoutObject.tv_ok)).click();
		String expectedtitle = "Your Mobile number was verified Successfully !";
		wait.until(ExpectedConditions.elementToBeClickable(logoutObject.login_welcome_message)).isDisplayed();

		String actualtitle = logoutObject.login_welcome_message.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		System.out.println("Logout Success");
	}
}
