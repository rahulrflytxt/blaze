package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebundles.CreateyourplanAirtimePage;
import com.safaricom.blazebundles.DataCallingPlanHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesDataCallingTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public DataCallingPlanHomePage datacallObject=new DataCallingPlanHomePage(driver);


	@Test
	public void SERVICES_DATACALLPLANSSERVICES_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		wait.until(ExpectedConditions.elementToBeClickable(servicesObject.datacall_click)).click();
		Thread.sleep(2000);
		String expectedtitledata="DATA & CALLING PLANS";
		String actualtitledata=datacallObject.datacallingtitle.getText();
		Assert.assertEquals(expectedtitledata,actualtitledata);
		Assert.assertEquals(true,datacallObject.airtimeBalance.isDisplayed(),"Airtime Balance is not displayed" );
		Assert.assertEquals(true,datacallObject.dataBundlesWithoutExpiry.isDisplayed(),"Data Bundles without Expiry is not displayed" );
		Assert.assertEquals(true,datacallObject.amountField.isDisplayed(),"Amount field is not displayed" );
		Thread.sleep(2000);
		driver.navigate().back();
	}
}
