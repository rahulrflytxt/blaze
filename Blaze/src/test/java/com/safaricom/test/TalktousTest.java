package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.talktous.FaqsHomePage;
import com.safaricom.talktous.LivechatHomepage;
import com.safaricom.talktous.ShareyourstoryPage;
import com.safaricom.talktous.TalktousHomepage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import junit.framework.Assert;

public class TalktousTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public TalktousHomepage talktousObject=new TalktousHomepage(driver);
	public LivechatHomepage livechatObject=new LivechatHomepage(driver);
	public ShareyourstoryPage sharestoryObject=new ShareyourstoryPage(driver);
	public FaqsHomePage faqsObject=new FaqsHomePage(driver);
	
	@Test
	public void BLAZETALKTOUS_TC_001() throws InterruptedException, IOException {
	try
	{
		homeObject.exploremsg.isDisplayed();
		
	}
	catch(Exception e)
	{
		driver.navigate().back();
	}
	Thread.sleep(4000);
	wait.until(ExpectedConditions.elementToBeClickable(homeObject.talktous)).click();
	wait.until(ExpectedConditions.elementToBeClickable(talktousObject.talktous_title)).isDisplayed();
	String expected="TALK TO US";
	String actual=talktousObject.talktous_title.getText();
	Assert.assertEquals(expected, actual);
	Assert.assertEquals(true, talktousObject.chatwithcustomercare.isDisplayed());
	Assert.assertEquals(true, talktousObject.shareyourstory.isDisplayed());
	Assert.assertEquals(true, talktousObject.faqs.isDisplayed());
	}
	
	@Test
	public void BLAZETALKTOUS_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.talktous.click();
			wait.until(ExpectedConditions.elementToBeClickable(talktousObject.talktous_title)).isDisplayed();
		}
		catch(Exception e)
		{
			System.out.println("Talk to us Page");
		}
		
		talktousObject.chatwithcustomercare.click();
		try
		{
			Thread.sleep(4000);
			if (livechatObject.certificatemsg.isDisplayed()) {
				livechatObject.certificateokbtn.click();
			}
		}
		catch(Exception e)
		{
			System.out.println("Live chat page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(livechatObject.livechat_title)).isDisplayed();
		String expected="LIVE CHAT";
		String actual=livechatObject.livechat_title.getText();
		Assert.assertEquals(expected, actual);
		wait.until(ExpectedConditions.elementToBeClickable(livechatObject.header)).isDisplayed();
		Assert.assertEquals(true, livechatObject.header.isDisplayed());
		Thread.sleep(2000);
		driver.navigate().back();
	}

	@Test
	public void BLAZETALKTOUS_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.talktous.click();	
			wait.until(ExpectedConditions.elementToBeClickable(talktousObject.talktous_title)).isDisplayed();

		}
		catch(Exception e)
		{
			System.out.println("Talk to us Page");
		}
		talktousObject.shareyourstory.click();
		wait.until(ExpectedConditions.elementToBeClickable(sharestoryObject.shareyourstory_title)).isDisplayed();
		String expected="SHARE STORY";
		String actual=sharestoryObject.shareyourstory_title.getText();
		Assert.assertEquals(expected, actual);
		Thread.sleep(4000);
		driver.navigate().back();
	}

	@Test
	public void BLAZETALKTOUS_TC_004() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.talktous.click();	
			wait.until(ExpectedConditions.elementToBeClickable(talktousObject.talktous_title)).isDisplayed();

		}
		catch(Exception e)
		{
			System.out.println("Talk to us Page");
		}
		talktousObject.faqs.click();
		wait.until(ExpectedConditions.elementToBeClickable(faqsObject.faqs_title)).isDisplayed();
		String expected="FAQ'S";
		String actual=faqsObject.faqs_title.getText();
		Assert.assertEquals(expected, actual);
		Thread.sleep(4000);
		driver.navigate().back();
	}
}
