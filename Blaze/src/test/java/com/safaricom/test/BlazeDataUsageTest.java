package com.safaricom.test;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.datausage.DatausageHomePage;
import com.safaricom.home.HomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BlazeDataUsageTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public DatausageHomePage blazedatausageObject=new DatausageHomePage(driver);
	
	

	@Test
	public void BLAZEDATAUSAGE_TC_001() throws InterruptedException, IOException {
		Thread.sleep(4000);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazedatausage)).click();	
		wait.until(ExpectedConditions.elementToBeClickable(blazedatausageObject.title)).isDisplayed();
		String expected="Data Usage";
		String actual=blazedatausageObject.title.getText();
		Assert.assertEquals(expected, actual);
		blazedatausageObject.fromdate.click();
		blazedatausageObject.prevbtn.click();
		blazedatausageObject.prevbtn.click();
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.view.View\")).scrollIntoView("
				+ "new UiSelector().text(\"1\"))").click();
		blazedatausageObject.okbtn.click();
		blazedatausageObject.todate.click();
		blazedatausageObject.okbtn.click();
		Thread.sleep(3000);
		driver.navigate().back();
	
	}
}
