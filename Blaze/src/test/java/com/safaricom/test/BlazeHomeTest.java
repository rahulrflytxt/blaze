package com.safaricom.test;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.login.GenerateOtpPage;
import com.safaricom.login.GeneratePinPage;
import com.safaricom.login.PermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BlazeHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homepageObject=new HomePage(driver);
	
	@Test
	public void BLAZEHOME_TC_001() throws InterruptedException, IOException
	{
	
		Assert.assertEquals(true, homepageObject.welcomemsg.isDisplayed(),"Welcome message is not displayed");
		Assert.assertEquals(true, homepageObject.name.isDisplayed(),"Name is not displayed");
		String expected="Explore the BLAZE world";
		String actual=homepageObject.exploremsg.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, homepageObject.beyourboss.isDisplayed(),"Be your boss icon is not displayed");
		Assert.assertEquals(true, homepageObject.blazebundles.isDisplayed(),"Blaze Bundles icon is not displayed");
		Assert.assertEquals(true, homepageObject.blazedatausage.isDisplayed(),"Blaze data usage icon is not displayed");
		Assert.assertEquals(true, homepageObject.blazebonga.isDisplayed(),"Blaze Bonga icon is not displayed");	
		Assert.assertEquals(true, homepageObject.blazempesa.isDisplayed(),"Blaze Mpesa icon is not displayed");
		Assert.assertEquals(true, homepageObject.blazedevices.isDisplayed(),"Blaze Devices icon is not displayed");
		Assert.assertEquals(true, homepageObject.talktous.isDisplayed(),"Talk to us icon is not displayed");	}

}
