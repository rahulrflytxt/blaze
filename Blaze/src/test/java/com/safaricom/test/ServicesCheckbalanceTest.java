package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebundles.CheckBalanceHomePage;
import com.safaricom.blazebundles.DataCallingPlanHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.DateselectPage;
import com.safaricom.services.ServicesCheckbalanceHomePage;
import com.safaricom.services.ServicesFullstatementPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesRedeembongaPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesCheckbalanceTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public ServicesCheckbalanceHomePage checkbalancehomeObject=new ServicesCheckbalanceHomePage(driver);
	public ServicesRedeembongaPage redeembongaObject=new ServicesRedeembongaPage(driver);
	public DataCallingPlanHomePage datacallObject=new DataCallingPlanHomePage(driver);
	public ServicesFullstatementPage fullstatementPageObject=new ServicesFullstatementPage(driver);
	public DateselectPage dateselectpageObject=new DateselectPage(driver);

	@Test
	public void SERVICES_CHECKBALANCE_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();
		servicesObject.checkbalance_click.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(checkbalancehomeObject.checkbalancetitle)).isDisplayed();
		String expectedtitle="Check Balance";
		String checkbalancetitle=checkbalancehomeObject.checkbalancetitle.getText();
		Assert.assertEquals(checkbalancetitle, expectedtitle);
		Assert.assertEquals(true, checkbalancehomeObject.date.isDisplayed());
		Assert.assertEquals(true, checkbalancehomeObject.balance.isDisplayed());
		Assert.assertEquals(true, checkbalancehomeObject.btn_redeem_bonga_click.isDisplayed());
		Assert.assertEquals(true, checkbalancehomeObject.btn_buy_data_click.isDisplayed());
		Assert.assertEquals(true, checkbalancehomeObject.buttonFullStatement_click.isDisplayed());
		Assert.assertEquals(true, checkbalancehomeObject.buttonMiniStatement_click.isDisplayed());
			}
	
	@Test
	public void SERVICES_CHECKBALANCE_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();
			servicesObject.checkbalance_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		checkbalancehomeObject.btn_redeem_bonga_click.click();
		String expected_maintitle = "Bonga Points";
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.title_bonga_points)).isDisplayed();
		String actual_maintitle = redeembongaObject.title_bonga_points.getText();
		System.out.println(expected_maintitle + actual_maintitle);
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		redeembongaObject.redeem_option.click();
		redeembongaObject.redeem_option_select.click();
		redeembongaObject.amount_field.click();
		redeembongaObject.amount_field_select.click();
		redeembongaObject.redeem_button.click();
		redeembongaObject.ok_click.click();
		Thread.sleep(1000);
		String expected_confirmationmessage = "Your request to redeem is successful. Thank you for staying with Safaricom.";
		String actual_confirmationmessage = redeembongaObject.final_confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redeembongaObject.ok_click)).click();
		Thread.sleep(2000);
		driver.navigate().back();
		

}
	@Test
	public void SERVICES_CHECKBALANCE_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();
			servicesObject.checkbalance_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		checkbalancehomeObject.btn_buy_data_click.click();
		String expectedtitledata = "DATA & CALLING PLANS";
		wait.until(ExpectedConditions.elementToBeClickable(datacallObject.datacallingtitle)).isDisplayed();
		String actualtitledata=datacallObject.datacallingtitle.getText();
		Assert.assertEquals(expectedtitledata,actualtitledata);
		Assert.assertEquals(true,datacallObject.airtimeBalance.isDisplayed(),"Airtime Balance is not displayed" );
		Assert.assertEquals(true,datacallObject.dataBundlesWithoutExpiry.isDisplayed(),"Data Bundles without Expiry is not displayed" );
		Assert.assertEquals(true,datacallObject.amountField.isDisplayed(),"Amount field is not displayed" );
		Thread.sleep(3000);
		driver.navigate().back();
	}
	@Test
	public void SERVICES_CHECKBALANCE_TC_004() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();
			servicesObject.checkbalance_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		checkbalancehomeObject.buttonFullStatement_click.click();

		String expected_maintitle = "Prepaid Usage Statement";
		String actual_maintitle = fullstatementPageObject.ministatement_labeltitle.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, fullstatementPageObject.inputEmail.isDisplayed(), "Email field is not displayed");
		fullstatementPageObject.inputEmail.sendKeys("rahul.r@flytxt.com");
		fullstatementPageObject.labelFromDate.click();
		dateselectpageObject.ok_button.click();
		fullstatementPageObject.labelToDate.click();
		dateselectpageObject.ok_button.click();
		driver.navigate().back();
		fullstatementPageObject.request_btn.click();
		wait.until(ExpectedConditions.elementToBeClickable(fullstatementPageObject.inputTransactionCode)).isDisplayed();
		fullstatementPageObject.cancel_btn.click();
	}
	@Test
	public void SERVICES_CHECKBALANCE_TC_005() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			wait.until(ExpectedConditions.elementToBeClickable(servicesObject.services_title)).isDisplayed();
			servicesObject.checkbalance_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		checkbalancehomeObject.buttonMiniStatement_click.click();
		dateselectpageObject.ok.click();
		Thread.sleep(2500);
		driver.navigate().back();
		
	}
}
