package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.BillmanagerHomePage;
import com.safaricom.mpesa.LipanaHomePage;
import com.safaricom.mpesa.MpesaHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BillmanagerTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public BillmanagerHomePage billObject=new BillmanagerHomePage(driver);
	
	@Test
	public void BLAZEBILLMANAGER_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.billmanagerClick)).isDisplayed();	
		mpesahomeObject.billmanagerClick.click();
		String expectedtitle = "MY BILLS";
		String billmanager_title = billObject.billmanager_title.getText();
		System.out.println(billmanager_title);
		Assert.assertEquals(billmanager_title, expectedtitle);
		Assert.assertEquals(true, billObject.populartab.isDisplayed());
		Assert.assertEquals(true, billObject.frequenttab.isDisplayed());
		Assert.assertEquals(true, billObject.managetab.isDisplayed());
		billObject.frequenttab.click();
		Thread.sleep(1000);
		billObject.managetab.click();
		Thread.sleep(1000);

	}
	@Test
	public void BLAZEBILLMANAGER_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.billmanagerClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		billObject.populartab.click();
		wait.until(ExpectedConditions.elementToBeClickable(billObject.firstpopbill)).isDisplayed();	
		billObject.firstpopbill.click();
		Assert.assertEquals(true, billObject.paybiltitle.isDisplayed());
		Assert.assertEquals(true, billObject.accnumber.isDisplayed());
		Assert.assertEquals(true, billObject.continuebtn.isDisplayed());
		billObject.accnumber.sendKeys("790771777");
		driver.navigate().back();
		billObject.continuebtn.click();
		wait.until(ExpectedConditions.elementToBeClickable(billObject.msg)).isDisplayed();	
		String expected="Dear Customer we are unable to complete your request. Kindly enter the correct account details.";
		String actual=billObject.msg.getText();
		Assert.assertEquals(actual, expected);
		billObject.okbtn.click();
		Thread.sleep(2000);
		driver.navigate().back();


	}
}
;
