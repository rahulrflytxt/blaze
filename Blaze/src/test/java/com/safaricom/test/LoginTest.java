package com.safaricom.test;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.login.GenerateOtpPage;
import com.safaricom.login.GeneratePinPage;
import com.safaricom.login.PermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class LoginTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	File csv_path = new File(System.getProperty("user.dir") + "/Blaze.csv");
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	PermissionPage permissionObject=new PermissionPage(driver);
	public GeneratePinPage generatepinObject=new GeneratePinPage(driver);
	public GenerateOtpPage generateotpObject=new GenerateOtpPage(driver);
	public HomePage homepageObject=new HomePage(driver);
	
	/*
	 * Verify whether user is able to login when provided with valid user name and
	 * password and accepted "Terms & Conditions".
	 */

	@Test
	public void LOGIN_TC_001() throws InterruptedException, IOException {
		driver.resetApp();
		wait.until(ExpectedConditions.elementToBeClickable(permissionObject.allow)).click();
		wait.until(ExpectedConditions.elementToBeClickable(permissionObject.allow)).click();
		wait.until(ExpectedConditions.elementToBeClickable(generatepinObject.accountNumber)).click();
		// Csv Reader

		CSVReader reader = new CSVReader(new FileReader(csv_path));
		String[] csvCell;
		// while loop will be executed till the last line In CSV.
		while ((csvCell = reader.readNext()) != null) {
			String segment = csvCell[0];
			if (segment.equals("blaze")) {
				String msidnSegment = csvCell[1];
				wait.until(ExpectedConditions.elementToBeClickable(generatepinObject.accountNumber)).sendKeys("0"+msidnSegment);
				driver.navigate().back();
				wait.until(ExpectedConditions.elementToBeClickable(generatepinObject.submitButton)).click();
				wait.until(ExpectedConditions.elementToBeClickable(generateotpObject.otp)).sendKeys("1234");
				driver.navigate().back();
				wait.until(ExpectedConditions.elementToBeClickable(generateotpObject.termConditionClick)).click();
				wait.until(ExpectedConditions.elementToBeClickable(generateotpObject.OtpLoginButton)).click();
				Thread.sleep(1000);
				try
				{
					Thread.sleep(3000);
				if (homepageObject.sfctitle.getText().equals("MENU")) {
					wait.until(ExpectedConditions.elementToBeClickable(homepageObject.menutab_click)).click();
					MobileElement elementToClick = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
							+ "new UiSelector().text(\"Back to Blaze\"))");
					elementToClick.click();
				}
				wait.until(ExpectedConditions.elementToBeClickable(homepageObject.ok_button)).click();
				Thread.sleep(2500);
				wait.until(ExpectedConditions.elementToBeClickable(homepageObject.okbutton)).click();
				}
				catch(Exception e)
				{
					System.out.print("Login");
				}
				wait.until(ExpectedConditions.elementToBeClickable(homepageObject.welcomemsg)).isDisplayed();
				wait.until(ExpectedConditions.elementToBeClickable(homepageObject.welcomemsg)).click();

				Assert.assertEquals(true, homepageObject.welcomemsg.isDisplayed(),"Blaze Segment not loaded");

			}
		}
	}
}
