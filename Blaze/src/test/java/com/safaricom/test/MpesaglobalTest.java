package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.BankAccountPage;
import com.safaricom.mpesa.CostEstimatorHomePage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.mpesa.MpesaglobalHomePage;
import com.safaricom.mpesa.OptoutPage;
import com.safaricom.mpesa.PaypalPage;
import com.safaricom.mpesa.SendtoMobilePage;
import com.safaricom.mpesa.WesternUnionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MpesaglobalTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public MpesaglobalHomePage mpesaglobalhomeobject=new MpesaglobalHomePage(driver);
	public SendtoMobilePage sendmobileObject=new SendtoMobilePage(driver);
	public BankAccountPage bankaccountObject=new BankAccountPage(driver);
	public WesternUnionPage westernObject= new WesternUnionPage(driver);
	public PaypalPage paypalObject=new PaypalPage(driver);
	public CostEstimatorHomePage costestimatorObject=new CostEstimatorHomePage(driver);
	public OptoutPage optoutObject=new OptoutPage(driver);
	
	
	@Test
	public void BLAZEMPESAGLOBAL_TC_001() throws InterruptedException, IOException {
		
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.mpesaglobalClick)).isDisplayed();	
		mpesahomeObject.mpesaglobalClick.click();
		String expectedtitle = "M-PESA GLOBAL";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);	
		Assert.assertEquals(true,mpesaglobalhomeobject.sendtomobile.isDisplayed(),"Send to Mobile Number is not displayed" );
		Assert.assertEquals(true,mpesaglobalhomeobject.bankaccount.isDisplayed(),"Bank Account is not displayed" );
		Assert.assertEquals(true,mpesaglobalhomeobject.westernunionlocation.isDisplayed(),"Western Union Location is not displayed" );
		Assert.assertEquals(true,mpesaglobalhomeobject.paypal.isDisplayed(),"Paypal is not displayed" );
		Assert.assertEquals(true,mpesaglobalhomeobject.costestimator.isDisplayed(),"Cost Estimator is not displayed" );	
		Assert.assertEquals(true,mpesaglobalhomeobject.optoutbtn.isDisplayed(),"Opt Out button is not displayed" );	

	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.sendtomobile)).click();
		String expectedtitle = "Mobile Number";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true,sendmobileObject.L1.get(0).isDisplayed(),"Country is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.L1.get(0))).click();
		Assert.assertEquals(true,sendmobileObject.countrytitle.isDisplayed(),"Country Title is not displayed" );
		Assert.assertEquals(true,sendmobileObject.L1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertFalse(sendmobileObject.countrycode.getText().isEmpty(), "Country code not displayed");
		Assert.assertEquals(true, sendmobileObject.phonenumber.isDisplayed(),"Phone number field not displayed"); 
		Assert.assertFalse(sendmobileObject.currency.getText().isEmpty(), "Currency not displayed");
	
		Assert.assertEquals(true, sendmobileObject.amount.isDisplayed(),"Amount field not displayed"); 
		Assert.assertEquals(true,sendmobileObject.L1.get(1).isDisplayed(),"Source of Fund not displayed" );
		Assert.assertEquals(true,sendmobileObject.continuebtn.isDisplayed(),"Continue button is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.phonenumber)).sendKeys("790771777");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.L2.get(1))).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.continuebtn)).click();
		Thread.sleep(2500);		
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.confirmheader)).isDisplayed();
		Assert.assertEquals(true,sendmobileObject.confirmheader.isDisplayed(),"Confirm Header is not displayed" );
		Assert.assertEquals(true,sendmobileObject.ConfirmL1.get(0).isDisplayed(),"Send Money to is not displayed" );
		Assert.assertEquals(true,sendmobileObject.ConfirmL1.get(1).isDisplayed(),"Amount is not displayed" );
		Assert.assertEquals(true,sendmobileObject.ConfirmL1.get(2).isDisplayed(),"Exchange Rate is not displayed" );
		Assert.assertEquals(true,sendmobileObject.ConfirmL1.get(3).isDisplayed(),"Amount is not displayed" );
		Assert.assertEquals(true,sendmobileObject.ConfirmL1.get(4).isDisplayed(),"Transaction cost is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.okbtn)).click();
		String expected_label_other = "Please wait to enter M-PESA PIN.";
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.labelmsg)).isDisplayed();

		String actual_label_other = sendmobileObject.labelmsg.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(sendmobileObject.lastokbtn)).click();

	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_004() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.bankaccount)).click();
		String expectedtitle = "BANK ACCOUNT";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true, bankaccountObject.subtitle_Countrydetails.isDisplayed(),"Country Details title is not displayed");
		Assert.assertEquals(true, bankaccountObject.subtitle_Receiverdetails.isDisplayed(),"Receiver Details title is not displayed");
		Assert.assertEquals(true, bankaccountObject.firstname.isDisplayed(),"First name is not displayed");
		Assert.assertEquals(true, bankaccountObject.middlename.isDisplayed(),"Middle name is not displayed");
		Assert.assertEquals(true, bankaccountObject.lastname.isDisplayed(),"Last name is not displayed");
		Assert.assertEquals(true, bankaccountObject.address.isDisplayed(),"Address is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.countrydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.L1.get(1))).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.curencydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.L1.get(0))).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.firstname)).sendKeys("TestFirst");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.middlename)).sendKeys("TestMiddle");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.lastname)).sendKeys("TestLast");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.address)).sendKeys("TestAddress");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.nextbtn)).click();

		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.bankname)).sendKeys("BankTest");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.swiftcode)).sendKeys("TestSwift");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.accountnumber)).sendKeys("TestAccount");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.amount)).sendKeys("2000");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.reference)).sendKeys("TestReference");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.nextbtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.L2.get(1))).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.purpose)).sendKeys("PurposeTest");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.submitbtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.confrimationheader)).isDisplayed();

		Assert.assertEquals(true, bankaccountObject.confrimationheader.isDisplayed(),"Confirmation header is not displayed");
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(1).isDisplayed(),"Name is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(2).isDisplayed(),"Bank Name is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(3).isDisplayed(),"Bank Account is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(4).isDisplayed(),"Refernce is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(5).isDisplayed(),"Exchange Rate is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(6).isDisplayed(),"Amount to send is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(7).isDisplayed(),"Amount to receive is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(8).isDisplayed(),"Transaction cost is not displayed" );
		Assert.assertEquals(true,bankaccountObject.confirmL1.get(9).isDisplayed(),"Total amount is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.nextbtn)).click();
		String expected_label_other = "Please wait to Enter M-PESA PIN";
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.labelmsg)).isDisplayed();

		String actual_label_other = bankaccountObject.labelmsg.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.lastokbtn)).click();
	}
	
	@Test
	public void BLAZEMPESAGLOBAL_TC_005() throws InterruptedException, IOException {
		
	}
	
	@Test
	public void BLAZEMPESAGLOBAL_TC_006() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.westernunionlocation)).click();
		String expectedtitle = "WESTERN UNION LOCATION";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true, westernObject.subtitle_Countrydetails.isDisplayed(),"Country Details title is not displayed");
		Assert.assertEquals(true, westernObject.subtitle_Receiverdetails.isDisplayed(),"Receiver Details title is not displayed");
		Assert.assertEquals(true, westernObject.firstname.isDisplayed(),"First name is not displayed");
		Assert.assertEquals(true, westernObject.middlename.isDisplayed(),"Middle name is not displayed");
		
		Assert.assertEquals(true, westernObject.lastname.isDisplayed(),"Last name is not displayed");
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"Amount*\"))");
		Assert.assertEquals(true, westernObject.amount.isDisplayed(),"Amount is not displayed");
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"Country Details\"))");
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.countrydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.L1.get(4))).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.curencydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.L1.get(0))).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.firstname)).sendKeys("TestFirst");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.middlename)).sendKeys("TestMiddle");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.lastname)).sendKeys("TestLast");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.nextbtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.L2.get(1))).click();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.purpose)).sendKeys("PurposeTest");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.submitbtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(bankaccountObject.confrimationheader)).isDisplayed();

		Assert.assertEquals(true, westernObject.confrimationheader.isDisplayed(),"Confirmation header is not displayed");
		Assert.assertEquals(true,westernObject.confirmL1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertEquals(true,westernObject.confirmL1.get(1).isDisplayed(),"Name is not displayed" );

		Assert.assertEquals(true,westernObject.confirmL1.get(2).isDisplayed(),"Exchange Rate is not displayed" );
		Assert.assertEquals(true,westernObject.confirmL1.get(3).isDisplayed(),"Amount to send is not displayed" );
		Assert.assertEquals(true,westernObject.confirmL1.get(4).isDisplayed(),"Amount to receive is not displayed" );
		Assert.assertEquals(true,westernObject.confirmL1.get(5).isDisplayed(),"Transaction cost is not displayed" );
		Assert.assertEquals(true,westernObject.confirmL1.get(6).isDisplayed(),"Total amount is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.nextbtn)).click();
		String expected_label_other = "Please wait to Enter M-PESA PIN";
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.labelmsg)).isDisplayed();

		String actual_label_other = westernObject.labelmsg.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(westernObject.lastokbtn)).click();

	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_007() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.paypal)).click();
		String expectedtitle = "PAYPAL";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true, paypalObject.paypaltitle.isDisplayed(),"Paypal title is not displayed");
		Assert.assertEquals(true, paypalObject.paypalicon.isDisplayed(),"Paypal icon is not displayed");
		Assert.assertEquals(true, paypalObject.L1.get(0).isDisplayed(),"Topup option is not displayed");
		Assert.assertEquals(true, paypalObject.L1.get(1).isDisplayed(),"Withdraw option is not displayed");
		Assert.assertEquals(true, paypalObject.currency.isDisplayed(),"Currency is not displayed");
		Assert.assertEquals(true, paypalObject.amount.isDisplayed(),"Amount is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.L1.get(0))).click();
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.continuebtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.confrimationheader)).isDisplayed();

		Assert.assertEquals(true, paypalObject.confrimationheader.isDisplayed(),"Confirmation header is not displayed");
		Assert.assertEquals(true,paypalObject.confirmL1.get(0).isDisplayed(),"Send Money to is not displayed" );
		Assert.assertEquals(true,paypalObject.confirmL1.get(1).isDisplayed(),"Amount is not displayed" );
		Assert.assertEquals(true,paypalObject.confirmL1.get(2).isDisplayed(),"Exchange Rate is not displayed" );
		Assert.assertEquals(true,paypalObject.confirmL1.get(3).isDisplayed(),"Amount is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.nextbtn)).click();
		String expected_label_other = "Please wait to enter M-PESA PIN.";
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.labelmsg)).isDisplayed();

		String actual_label_other = paypalObject.labelmsg.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.lastokbtn)).click();
		driver.navigate().back();
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_008() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		mpesahomeObject.mpesaglobalClick.click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.paypal)).click();
		String expectedtitle = "PAYPAL";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = mpesaglobalhomeobject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true, paypalObject.paypaltitle.isDisplayed(),"Paypal title is not displayed");
		Assert.assertEquals(true, paypalObject.paypalicon.isDisplayed(),"Paypal icon is not displayed");
		Assert.assertEquals(true, paypalObject.L1.get(0).isDisplayed(),"Topup option is not displayed");
		Assert.assertEquals(true, paypalObject.L1.get(1).isDisplayed(),"Withdraw option is not displayed");
		Assert.assertEquals(true, paypalObject.currency.isDisplayed(),"Currency is not displayed");
		Assert.assertEquals(true, paypalObject.amount.isDisplayed(),"Amount is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.L1.get(2))).click();
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.continuebtn)).click();
		String expected="You will be redirected to PayPal M-PESA page to complete this transaction. Please note there will be No Charges incurred.";
		String actual=paypalObject.labelmsg.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(paypalObject.canclbtn)).click();
		driver.navigate().back();
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_009() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.costestimator)).click();
		String expectedtitle = "Cost Estimator";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = costestimatorObject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true,costestimatorObject.sendtomobile.isDisplayed(),"Send to Mobile Number is not displayed" );
		Assert.assertEquals(true,costestimatorObject.bankaccount.isDisplayed(),"Bank Account is not displayed" );
		Assert.assertEquals(true,costestimatorObject.westernunionlocation.isDisplayed(),"Western Union Location is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.sendtomobile)).click();
		Assert.assertEquals(true, costestimatorObject.sendMobileSubtitle.isDisplayed(),"SendMobile title is not displayed");
		Assert.assertEquals(true, costestimatorObject.countrydrop.isDisplayed(),"Country dropdown is not displayed");
		Assert.assertEquals(true, costestimatorObject.amount.isDisplayed(),"Amount field is not displayed");
		Assert.assertEquals(true, costestimatorObject.continuebtn.isDisplayed(),"Calculate button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.countrydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.L1.get(1))).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.continuebtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).isDisplayed();

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(1).isDisplayed(),"Amount to send not displayed" );

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(2).isDisplayed(),"Transaction cost is not displayed" );
		Assert.assertEquals(true,costestimatorObject.closebtn.isDisplayed(),"Close button is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).click();
		driver.navigate().back();
		driver.navigate().back();
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_010() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.costestimator)).click();
		String expectedtitle = "Cost Estimator";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = costestimatorObject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true,costestimatorObject.sendtomobile.isDisplayed(),"Send to Mobile Number is not displayed" );
		Assert.assertEquals(true,costestimatorObject.bankaccount.isDisplayed(),"Bank Account is not displayed" );
		Assert.assertEquals(true,costestimatorObject.westernunionlocation.isDisplayed(),"Western Union Location is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.bankaccount)).click();
		Assert.assertEquals(true, costestimatorObject.bankaccountSubtitle.isDisplayed(),"Bank Account title is not displayed");
		Assert.assertEquals(true, costestimatorObject.countrydrop.isDisplayed(),"Country dropdown is not displayed");
		Assert.assertEquals(true, costestimatorObject.curencydrop.isDisplayed(),"Currency dropdown is not displayed");
		Assert.assertEquals(true, costestimatorObject.amount.isDisplayed(),"Amount field is not displayed");
		Assert.assertEquals(true, costestimatorObject.continuebtn.isDisplayed(),"Calculate button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.countrydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.L1.get(1))).click();

		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.curencydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.L1.get(0))).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.continuebtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).isDisplayed();

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(1).isDisplayed(),"Exchange rate not displayed" );

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(2).isDisplayed(),"Amount to send is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(3).isDisplayed(),"Amount to receive is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(4).isDisplayed(),"Transaction cost is not displayed" );

		Assert.assertEquals(true,costestimatorObject.closebtn.isDisplayed(),"Close button is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).click();
		driver.navigate().back();
		driver.navigate().back();
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_011() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.costestimator)).click();
		String expectedtitle = "Cost Estimator";
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.mpesaglobal_title)).isDisplayed();
		String mpesaglobal_title = costestimatorObject.mpesaglobal_title.getText();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
		Assert.assertEquals(true,costestimatorObject.sendtomobile.isDisplayed(),"Send to Mobile Number is not displayed" );
		Assert.assertEquals(true,costestimatorObject.bankaccount.isDisplayed(),"Bank Account is not displayed" );
		Assert.assertEquals(true,costestimatorObject.westernunionlocation.isDisplayed(),"Western Union Location is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.westernunionlocation)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.westernunionSubtitle)).isDisplayed();

		Assert.assertEquals(true, costestimatorObject.westernunionSubtitle.isDisplayed(),"Western Union title is not displayed");
		Assert.assertEquals(true, costestimatorObject.countrydrop.isDisplayed(),"Country dropdown is not displayed");
		Assert.assertEquals(true, costestimatorObject.curencydrop.isDisplayed(),"Currency dropdown is not displayed");
		Assert.assertEquals(true, costestimatorObject.amount.isDisplayed(),"Amount field is not displayed");
		Assert.assertEquals(true, costestimatorObject.continuebtn.isDisplayed(),"Calculate button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.countrydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.L1.get(1))).click();

		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.curencydrop)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.L1.get(0))).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.continuebtn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).isDisplayed();

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(0).isDisplayed(),"Country is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(1).isDisplayed(),"Exchange rate not displayed" );

		Assert.assertEquals(true,costestimatorObject.confirmL1.get(2).isDisplayed(),"Amount to send is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(3).isDisplayed(),"Amount to receive is not displayed" );
		Assert.assertEquals(true,costestimatorObject.confirmL1.get(4).isDisplayed(),"Transaction cost is not displayed" );

		Assert.assertEquals(true,costestimatorObject.closebtn.isDisplayed(),"Close button is not displayed" );
		wait.until(ExpectedConditions.elementToBeClickable(costestimatorObject.closebtn)).click();
		driver.navigate().back();
		driver.navigate().back();
	}
	@Test
	public void BLAZEMPESAGLOBAL_TC_012() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.mpesaglobalClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesaglobalhomeobject.optoutbtn)).click();	
		String expected="Are you sure you want to OPT OUT ?";
		String actual=optoutObject.optoutMsg.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(optoutObject.confirm)).click();	
		wait.until(ExpectedConditions.elementToBeClickable(optoutObject.submit)).click();	
		String expected_label_other = "Please wait to Enter M-PESA PIN";
		wait.until(ExpectedConditions.elementToBeClickable(optoutObject.labelmsg)).isDisplayed();

		String actual_label_other = optoutObject.labelmsg.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(optoutObject.lastokbtn)).click();
		
	}


}
