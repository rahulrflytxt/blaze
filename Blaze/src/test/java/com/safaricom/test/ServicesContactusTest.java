package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesContactusNewrequestPage;
import com.safaricom.services.ServicesContactusPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesStorelocatorPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesContactusTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public ServicesContactusPage servicecontactObject=new ServicesContactusPage(driver);
	public ServicesContactusNewrequestPage servicenewrequestObject=new ServicesContactusNewrequestPage(driver);
	@Test
	public void SERVICES_CONTACTUS_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
		

		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		servicesObject.contactus_click.click();
		Thread.sleep(2000);
		String expectedtitledata="Request";
		String actual_maintitle = servicecontactObject.contactUs_title.getText();
		Assert.assertEquals(actual_maintitle, expectedtitledata);
		Assert.assertEquals(true, servicecontactObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, servicecontactObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, servicecontactObject.img_insta_click.isDisplayed(), "Message is not displayed");

		
		
		String expected_subtitle = "Request";
		String actual_subtitle = servicenewrequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.requst_msg)).sendKeys("New Request");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(servicenewrequestObject.tv_ok)).click();
		driver.navigate().back();
		
	}
}
