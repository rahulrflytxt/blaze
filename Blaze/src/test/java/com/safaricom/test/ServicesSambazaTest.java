package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesSambazaAirtimePage;
import com.safaricom.services.ServicesSambazaDataPage;
import com.safaricom.services.ServicesSambazaPage;
import com.safaricom.services.ServicesSkizaPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesSambazaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public ServicesSambazaPage sambazaObject=new ServicesSambazaPage(driver);
	public ServicesSambazaAirtimePage sambazaairtimeObject=new ServicesSambazaAirtimePage(driver);
	public ServicesSambazaDataPage sambazadataObject=new ServicesSambazaDataPage(driver);
	
	@Test
	public void SERVICES_SAMBAZASERVICES_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
		

		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		servicesObject.sambaza_click.click();
		Thread.sleep(2000);
		String expectedtitledata="Sambaza Services";
		String actual_maintitle = sambazaObject.sambazaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expectedtitledata);
		Assert.assertEquals(true, sambazaObject.airtime_tv_bonga_points_title.isDisplayed(), "Airtime Balance title is not displayed");
		Assert.assertEquals(true, sambazaObject.tv_date_airtime.isDisplayed(), "Airtime Date is not displayed");
		Assert.assertEquals(true, sambazaObject.tv_amount_airtime.isDisplayed(), "Airtime Amount is not displayed");
		Assert.assertEquals(true, sambazaObject.airtimesambazaClick.isDisplayed(), "Airtime Sambaza button is not displayed");
		Assert.assertEquals(true, sambazaObject.tv_bonga_points_data.isDisplayed(), "Data Balance title is not displayed");
		Assert.assertEquals(true, sambazaObject.tv_date_data.isDisplayed(), "Data Date is not displayed");
		Assert.assertEquals(true, sambazaObject.tv_amount_data.isDisplayed(), "Data Amount is not displayed");
		Assert.assertEquals(true, sambazaObject.datasambazaClick.isDisplayed(), "Data Sambaza button is not displayed");
		
		
	}
	
	@Test
	public void SERVICES_SAMBAZASERVICES_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			servicesObject.sambaza_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Sambaza Page");
			}
		
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sambazaObject.airtimesambazaClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazaairtimeObject.airtime_edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaairtimeObject.airtime_et_pin)).sendKeys("100");
		wait.until(ExpectedConditions.elementToBeClickable(sambazaairtimeObject.airtime_btn_ok)).click();
		Thread.sleep(2000);
		String expected="Your request is successful. Thank you for staying with Safaricom.";
		String actual=sambazaairtimeObject.airtime_tv_message.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(sambazaairtimeObject.airtime_tv_ok)).click();

	}
	@Test
	public void SERVICES_SAMBAZASERVICES_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			servicesObject.sambaza_click.click();
		}
		catch(Exception e)
		{
			System.out.println("Sambaza Page");
		}
		
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sambazaObject.datasambazaClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sambazadataObject.data_edt_mobilenumber)).sendKeys("790771777");
		wait.until(ExpectedConditions.elementToBeClickable(sambazadataObject.data_et_pin)).sendKeys("30");
		wait.until(ExpectedConditions.elementToBeClickable(sambazadataObject.data_btn_ok)).click();
		Thread.sleep(2000);
		String expected="Your request is successful. Thank you for staying with Safaricom.";
		String actual=sambazadataObject.data_tv_message.getText();
		Assert.assertEquals(actual, expected);
		wait.until(ExpectedConditions.elementToBeClickable(sambazadataObject.data_tv_ok)).click();
		Thread.sleep(2000);
	}
}
