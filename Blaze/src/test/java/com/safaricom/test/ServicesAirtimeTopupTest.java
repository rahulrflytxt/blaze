package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebonga.BlazeBongaPage;
import com.safaricom.blazebonga.BlazebongaHomePage;
import com.safaricom.blazebundles.CreateyourplanAirtimePage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;

import com.safaricom.services.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesAirtimeTopupTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public CreateyourplanAirtimePage createairtimeObject=new CreateyourplanAirtimePage(driver);


	@Test
	public void SERVICES_AIRTIMETOPUP_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		wait.until(ExpectedConditions.elementToBeClickable(servicesObject.airtimetopup_click)).click();
		Thread.sleep(2000);
		String expected="BLAZE BUNDLES";
		String actual=servicesObject.services_title.getText();
		Assert.assertEquals(actual, expected);
		Assert.assertEquals(true, createairtimeObject.myaitimebalancesubtitle.isDisplayed(),"My airtime balance is not displayed");
		Assert.assertEquals(true, createairtimeObject.date.isDisplayed(),"Airtime date is not displayed");
		Assert.assertEquals(true, createairtimeObject.balance.isDisplayed(),"Balance is not displayed");
		Assert.assertEquals(true, createairtimeObject.topuptext.isDisplayed(),"Topup subtitle is not displayed");
		Assert.assertEquals(true, createairtimeObject.date2.isDisplayed(),"Topup date is not displayed");
		Assert.assertEquals(true, createairtimeObject.enter16text.isDisplayed(),"Enter pin text is not displayed");
		Assert.assertEquals(true, createairtimeObject.textfield16.isDisplayed(),"16 digit textfield is not displayed");
		Assert.assertEquals(true, createairtimeObject.okoajahazi.isDisplayed(),"Okoa Jahazi button is not displayed");
		Assert.assertEquals(true, createairtimeObject.loadairtime.isDisplayed(),"Load airtime button is not displayed");
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.Button\")).scrollIntoView("
				+ "new UiSelector().text(\"BUY AIRTIME WITH M-PESA\"))");
		Assert.assertEquals(true, createairtimeObject.buyairtimempesa.isDisplayed(),"Buy airtime with mpesa button is not displayed");
		driver.navigate().back();
	}
}
