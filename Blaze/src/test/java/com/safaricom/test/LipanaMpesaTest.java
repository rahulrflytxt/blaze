package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.LipanaHomePage;
import com.safaricom.mpesa.MpesaHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LipanaMpesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public LipanaHomePage lipanaObject=new LipanaHomePage(driver);
	
	@Test
	public void LIPANAMPESA_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(6000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.lipanampesaClick)).isDisplayed();	
		mpesahomeObject.lipanampesaClick.click();
		String expectedtitle = "Lipa na M-PESA";
		String lipanampesa_title = lipanaObject.lipanampesa_title.getText();
		System.out.println(lipanampesa_title);
		Assert.assertEquals(lipanampesa_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybillClick)).click();
		Assert.assertEquals(true, lipanaObject.paybill_edt_buss_no.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.BuygoodsClick)).click();
		Assert.assertEquals(true, lipanaObject.Buygoods_edt_till_no.isDisplayed());
		Assert.assertEquals(true, lipanaObject.Buygoods_edt_till_amount.isDisplayed());
		Assert.assertEquals(true, lipanaObject.Buygoods_continue.isDisplayed());

	
	}
	@Test
	public void LIPANAMPESA_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.lipanampesaClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		if (!(lipanaObject.paybillClick.isSelected())) {
			
			wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybillClick)).click();

		}
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybill_edt_buss_no)).sendKeys("12345");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybill_edt_account_number)).sendKeys("790771777");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybill_edt_bill_amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.paybill_continue)).click();
		Thread.sleep(3000);
		String expected_confirmation_title_paybill = "Confirmation";
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.lipanampesa_confirmation_title)).isDisplayed();

		String actual_confirmation_title_paybill = lipanaObject.lipanampesa_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_paybill, expected_confirmation_title_paybill);
		String txt_business_paybill = lipanaObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_paybill.isEmpty(),"Business is empty ");
		String txt_dialog_account_number_paybill = lipanaObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, txt_dialog_account_number_paybill.isEmpty(),"Account number is empty");
		String txt_dialog_amount_paybill = lipanaObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, txt_dialog_amount_paybill.isEmpty(),"Amount is empty");
		String tv_mpesa_mobile_value_paybill = lipanaObject.tv_mpesa_mobile_value.getText();
		Assert.assertEquals(false, tv_mpesa_mobile_value_paybill.isEmpty(),"Mobile value is empty");
		Assert.assertEquals(true, lipanaObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disbled");
		Assert.assertEquals(true, lipanaObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.txt_continue_dilaog)).click();
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();
	
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
		
	}
	@Test
	public void LIPANAMPESA_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.lipanampesaClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.BuygoodsClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.Buygoods_edt_till_no)).sendKeys("12122");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.Buygoods_edt_till_amount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.Buygoods_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title_buygoods = "Confirmation";
		String actual_confirmation_title_buygoods = lipanaObject.lipanampesa_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_buygoods, expected_confirmation_title_buygoods);
		String txt_business_buygoods = lipanaObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_buygoods.isEmpty(),"txt_business_buygoods is empty");
		String txt_dialog_amount_buygoods = lipanaObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, txt_dialog_amount_buygoods.isEmpty(),"txt_dialog_amount_buygoods is empty");
		String tv_mpesa_mobile_value_buygoods = lipanaObject.tv_mpesa_mobile_value.getText();
		Assert.assertEquals(false, tv_mpesa_mobile_value_buygoods.isEmpty(),"tv_mpesa_mobile_value_buygoods is empty");
		Assert.assertEquals(true, lipanaObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, lipanaObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(lipanaObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.ok_btn)).click();
	
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
	
	}
}
