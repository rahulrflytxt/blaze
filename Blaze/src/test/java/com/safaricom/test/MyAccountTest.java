package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.mpesa.MyaccountHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MyAccountTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public MyaccountHomePage myaccountObject=new MyaccountHomePage(driver);
	
	@Test
	public void BLAZEMYACCOUNT_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.myaccountClick)).isDisplayed();	
		mpesahomeObject.myaccountClick.click();
		String expectedtitle = "My Account";
		String lipanampesa_title = myaccountObject.title.getText();
		System.out.println(lipanampesa_title);
		Assert.assertEquals(lipanampesa_title, expectedtitle);
		Assert.assertEquals(true, myaccountObject.mpesaUnlock.isDisplayed(),"MpesaUnlock is not displayed");

	}
	
	@Test
	public void BLAZEMYACCOUNT_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.myaccountClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.mpesaUnlock)).click();
		Thread.sleep(1000);
		String expectedtitle = "UNLOCK YOUR M-PESA ACCOUNT";
		String mpesaunlock_title = myaccountObject.mpesaunlock_labelTitle.getText();
		Assert.assertEquals(mpesaunlock_title, expectedtitle);
		Assert.assertEquals(true, myaccountObject.inputPin.isDisplayed());
		Assert.assertEquals(true, myaccountObject.buttonPositive.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.inputPin)).sendKeys("123456"+"\n");

		
		wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.buttonPositive)).click();
		Thread.sleep(2500);
		try
		{
			myaccountObject.mpesaunlock_labelMessage.isDisplayed();
			String expectedMessage = "Dear Customer, We have accepted your request, you will be notified via SMS soon";
			String mpesaunlock_finalMessage = myaccountObject.mpesaunlock_labelMessage
					.getText();
			Assert.assertEquals(mpesaunlock_finalMessage, expectedMessage);
			wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.ok_button)).click();	
		}
		catch(Exception e)
		{
			myaccountObject.mpesaunlock_labelMessage1.isDisplayed();
			String expectedMessage = "Dear Customer, We have accepted your request, you will be notified via SMS soon";
			String mpesaunlock_finalMessage = myaccountObject.mpesaunlock_labelMessage1
					.getText();
			Assert.assertEquals(mpesaunlock_finalMessage, expectedMessage);
			wait.until(ExpectedConditions.elementToBeClickable(myaccountObject.ok_button)).click();	
		}
		
	}
	
		
}
