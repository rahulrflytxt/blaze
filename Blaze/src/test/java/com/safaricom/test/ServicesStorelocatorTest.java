package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesSambazaAirtimePage;
import com.safaricom.services.ServicesSambazaDataPage;
import com.safaricom.services.ServicesSambazaPage;
import com.safaricom.services.ServicesStorelocatorPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesStorelocatorTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public ServicesStorelocatorPage servicestoreObject=new ServicesStorelocatorPage(driver);
	
	@Test
	public void SERVICES_STORELOCATOR_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
		

		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		servicesObject.storelocator_click.click();
		Thread.sleep(2000);
		String expectedtitledata="Store Locator";
		String actual_maintitle = servicestoreObject.store_title.getText();
		Assert.assertEquals(actual_maintitle, expectedtitledata);
		Assert.assertEquals(true, servicestoreObject.textViewGroupName.isDisplayed(), "Group name is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(servicestoreObject.textViewGroupName)).click();
		Assert.assertEquals(true, servicestoreObject.txt_name.isDisplayed(), "Name is not displayed");
		Assert.assertEquals(true, servicestoreObject.txt_address.isDisplayed(), "Address is not displayed");
		Assert.assertEquals(true, servicestoreObject.txt_email.isDisplayed(), "Email is not displayed");
		Assert.assertEquals(true, servicestoreObject.txt_weekday.isDisplayed(), "Weekday is not displayed");
		Assert.assertEquals(true, servicestoreObject.txt_weekend.isDisplayed(), "Weekend is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(servicestoreObject.txt_name)).click();
		Thread.sleep(2500);
		driver.navigate().back();
		Thread.sleep(2000);
		driver.navigate().back();		
		
	}
}
