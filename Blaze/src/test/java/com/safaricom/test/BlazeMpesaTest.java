package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.MpesaHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BlazeMpesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	
	@Test
	public void BLAZEMPESA_TC_001() throws InterruptedException, IOException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.blazempesa)).click();
		String expectedtitle ="M-PESA";
		String sfctitle = mpesahomeObject.mpesa_title.getText();
		Assert.assertEquals(true, mpesahomeObject.mpesa_show_balance.isDisplayed());
		Assert.assertEquals(sfctitle, expectedtitle);
		Assert.assertEquals(true, mpesahomeObject.sendMoneyClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.withdraCashClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.buyairtimeClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.lipanampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.billmanagerClick.isDisplayed());

		Assert.assertEquals(true, mpesahomeObject.loansandsavingsClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.myaccountClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.fulizampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomeObject.mpesaglobalClick.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"Scan QR\"))");	
		Assert.assertEquals(true, mpesahomeObject.mpesa_buttonScanQR.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"M-PESA Statement\"))");	
		Assert.assertEquals(true, mpesahomeObject.mpesa_statement.isDisplayed());
	}
	
	
	
	
}
