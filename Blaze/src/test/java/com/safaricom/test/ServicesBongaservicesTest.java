package com.safaricom.test;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazebonga.BlazeBongaPage;
import com.safaricom.blazebonga.BlazebongaHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesBongaservicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public BlazebongaHomePage blazebongaObject=new BlazebongaHomePage(driver);
	public BlazeBongaPage bongaobject=new BlazeBongaPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);


	@Test
	public void SERVICES_BLAZEBONGA_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		wait.until(ExpectedConditions.elementToBeClickable(servicesObject.bongaservices_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebongaObject.title)).isDisplayed();
		String expectedtitle="BLAZE BONGA";
		String actualtitle=blazebongaObject.title.getText();
		Assert.assertEquals(expectedtitle,actualtitle);
		List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
		Assert.assertEquals(true, linkElements.get(1).isDisplayed(),"Blaze Bonga is not displayed");
		Assert.assertEquals(true, blazebongaObject.seeredemptionhistory.isDisplayed(),"See Redemption History is not displayed");
		Assert.assertEquals(true, blazebongaObject.seeaccumulationhistory.isDisplayed(),"See Accumulation History is not displayed");
	}
	@Test
	public void SERVICES_BLAZEBONGA_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			menuObject.menu_click.click();
			MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"Services\"))");
			service.click();
			Thread.sleep(2500);	
			wait.until(ExpectedConditions.elementToBeClickable(servicesObject.bongaservices_click)).click();

		}
		catch(Exception e)
		{
			System.out.println("Services Page");
		}
		
		List<MobileElement> linkElements = driver.findElements(By.xpath("//android.widget.TextView[@text='BLAZE BONGA']"));
		wait.until(ExpectedConditions.elementToBeClickable(linkElements.get(1))).click();
		try {
			Thread.sleep(5000);
			bongaobject.certificateokbtn.click();
		}
		catch(Exception e)
		{
			System.out.println("No certificate popup");
		}
		Assert.assertEquals(true, bongaobject.myblazebongatab.isDisplayed(),"My Blaze Bonga tab is not displayed");
		Assert.assertEquals(true, bongaobject.bongaofferstab.isDisplayed(),"Bonga Offers tab is not displayed");
		Assert.assertEquals(true, bongaobject.date.isDisplayed(),"Date is not displayed");
		Assert.assertEquals(true, bongaobject.curbalance.isDisplayed(),"Balance is not displayed");
		Assert.assertEquals(true, bongaobject.transferbtn.isDisplayed(),"Transfer button is not displayed");
		Assert.assertEquals(true, bongaobject.redeembtn.isDisplayed(),"Redeem button is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(bongaobject.bongaofferstab)).isDisplayed();
		bongaobject.bongaofferstab.click();
		Thread.sleep(2000);
		driver.navigate().back();
		Thread.sleep(2500);
		driver.navigate().back();
	}
	
}
