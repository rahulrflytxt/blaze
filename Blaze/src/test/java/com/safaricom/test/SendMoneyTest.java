package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.MpesaHomePage;
import com.safaricom.mpesa.SendMoneyHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

	public class SendMoneyTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public SendMoneyHomePage sendmoneyObject=new SendMoneyHomePage(driver)	;
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	
	
	@Test
	public void BLAZESENDMONEY_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.sendMoneyClick)).isDisplayed();
		mpesahomeObject.sendMoneyClick.click();
		String expectedtitle = "Send Money";
		String sendmoney_title = sendmoneyObject.SendMoney_title.getText();
		System.out.println(sendmoney_title);
		Assert.assertEquals(sendmoney_title, expectedtitle);
		Assert.assertEquals(true, sendmoneyObject.SendtoOnePhoneNumber.isDisplayed(),"Send to One Phone Number is not displayed");
		Assert.assertEquals(true, sendmoneyObject.SendtoOnePhoneAmount.isDisplayed(),"Send to One Amount is not displayed");
		Assert.assertEquals(true, sendmoneyObject.SendtoOneContinue.isDisplayed(),"Send to One continue button is not displayed");
		Assert.assertEquals(true, sendmoneyObject.SendtoOtherClick.isDisplayed(),"Send ot Other is not displayed");
		sendmoneyObject.SendtoOtherClick.click();
		Assert.assertEquals(true, sendmoneyObject.SendtoOtherPhoneNumber.isDisplayed(),"Send to Other Phone number is not displayed");
		Assert.assertEquals(true, sendmoneyObject.SendtoOtherPhoneAmount.isDisplayed(),"Send to Other Phone amount is not displayed");
		Assert.assertEquals(true, sendmoneyObject.SendToOtherContinue.isDisplayed(),"Send to Other Continue is not displayed");

		
		
		
	}
	
	@Test
	public void BLAZESENDMONEY_TC_002() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.sendMoneyClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.SendtoOneClick)).click();
		sendmoneyObject.SendtoOnePhoneNumber.sendKeys("790771777");
		driver.navigate().back();
		sendmoneyObject.SendtoOnePhoneAmount.sendKeys("200");
		driver.navigate().back();
		sendmoneyObject.SendtoOneContinue.click();
		Thread.sleep(2000);
		String expected_confirmation_title ="Confirmation";
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.confirmation_title)).isDisplayed();
		String actual_confirmation_title = sendmoneyObject.confirmation_title.getText();
		System.out.println(actual_confirmation_title);
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = sendmoneyObject.mobileno.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile vaue is empty");
		String mpesa_amount_value = sendmoneyObject.amount.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount value is empty");
		String transaction_cost = sendmoneyObject.transcost.getText();
		Assert.assertEquals(false, transaction_cost.isEmpty(),"Transaction Cost value is empty");
		Assert.assertEquals(true, sendmoneyObject.sendmoney_cancel.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, sendmoneyObject.sendmoney_continue.isEnabled(),"Continue button is displayed");
		sendmoneyObject.sendmoney_continue.click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinObject.nine_btn)).click();
		mpesapinObject.eight_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.four_btn.click();
		mpesapinObject.ok_btn.click();
	
		String expected_label = "You have entered the wrong PIN!";
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.final_confirmation_label1)).isDisplayed();

		String actual_label = mpesapinfinalObject.final_confirmation_label1.getText();
		Assert.assertEquals(actual_label, expected_label);
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(mpesapinfinalObject.ok_click1)).click();
		
		}
	

	@Test
	public void BLAZESENDMONEY_TC_003() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			mpesahomeObject.sendMoneyClick.click();

		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.SendtoOtherClick)).click();
		sendmoneyObject.SendtoOtherPhoneNumber.sendKeys("790771777");
		driver.navigate().back();
		sendmoneyObject.SendtoOtherPhoneAmount.sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.SendToOtherContinue)).click();
		Thread.sleep(3000);
		String expected_label_other = "Please wait to enter M-PESA PIN.";
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.final_confirmation_label)).isDisplayed();

		String actual_label_other = sendmoneyObject.final_confirmation_label.getText();
		Assert.assertEquals(actual_label_other, expected_label_other);
		wait.until(ExpectedConditions.elementToBeClickable(sendmoneyObject.errormsg_close)).click();
		driver.navigate().back();

		
	}
	
	
}
	
	
