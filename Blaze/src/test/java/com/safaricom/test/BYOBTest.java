package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.beyourboss.BeyourbossHomePage;
import com.safaricom.beyourboss.MentorProfilesPage;
import com.safaricom.beyourboss.MentorProfilesSecondPage;
import com.safaricom.beyourboss.SummitsPage;
import com.safaricom.beyourboss.TvshowPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class BYOBTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	BeyourbossHomePage byobObject=new BeyourbossHomePage(driver);
	HomePage homeObject=new HomePage(driver);
	MentorProfilesPage mentorObject=new MentorProfilesPage(driver);
	MentorProfilesSecondPage mentor2Object=new MentorProfilesSecondPage(driver);
	SummitsPage summitsObject=new SummitsPage(driver);
	TvshowPage tvshowObject=new TvshowPage(driver);
	
	@Test
	public void BEYOURBOSS_TC_001() throws InterruptedException, IOException {
		
		wait.until(ExpectedConditions.elementToBeClickable(homeObject.beyourboss)).click();
		wait.until(ExpectedConditions.elementToBeClickable(byobObject.beyorbosstitle)).isDisplayed();
		String expectedtitle="BYOB";
		String actualtitle=byobObject.beyorbosstitle.getText();
		Assert.assertEquals(expectedtitle,actualtitle);
		Assert.assertEquals(true, byobObject.mentorprofiles.isDisplayed(),"Mentor Profiles icon is not displayed");
		Assert.assertEquals(true, byobObject.summits.isDisplayed(),"Summits icon is not displayed");
		Assert.assertEquals(true, byobObject.tvshow.isDisplayed(),"TV Show icon is not displayed");
	}
	@Test
	public void BEYOURBOSS_TC_002() throws InterruptedException, IOException {
		try
		{
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.beyourboss)).click();

		}
		catch(Exception e)
		{
			System.out.println("Be your own Boss Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(byobObject.mentorprofiles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mentorObject.beyorbosstitle)).isDisplayed();
		Assert.assertEquals(true, mentorObject.beyorbosstitle.isDisplayed(),"Title is not displayed");
		Assert.assertEquals(true, mentorObject.mentor1.isDisplayed(),"Mentor profile not displayed");
		mentorObject.mentor1.click();
		Assert.assertEquals(true, mentor2Object.beyorbosstitle.isDisplayed(),"Title is not displayed");
		Assert.assertEquals(true, mentor2Object.mentortitle.isDisplayed(),"Mentor title is not displayed");
		Assert.assertEquals(true, mentor2Object.mentorimage.isDisplayed(),"Mentor image is not displayed");
		Assert.assertEquals(true, mentor2Object.nextbtn.isDisplayed(),"Next Button is not displayed");
		mentor2Object.nextbtn.click();
		Assert.assertEquals(true, mentor2Object.beyorbosstitle.isDisplayed(),"Title is not displayed");
		Assert.assertEquals(true, mentor2Object.mentortitle.isDisplayed(),"Mentor title is not displayed");
		Assert.assertEquals(true, mentor2Object.mentorimage.isDisplayed(),"Mentor image is not displayed");
		Assert.assertEquals(true, mentor2Object.nextbtn.isDisplayed(),"Next Button is not displayed");
		Assert.assertEquals(true, mentor2Object.prevbtn.isDisplayed(),"Previous Button is not displayed");
		mentor2Object.prevbtn.click();
		Thread.sleep(5000);
		driver.navigate().back();
		driver.navigate().back();
	}
	

	@Test
	public void BEYOURBOSS_TC_003() throws InterruptedException, IOException {
		try
		{
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.beyourboss)).click();

		}
		catch(Exception e)
		{
			System.out.println("Be your own Boss Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(byobObject.summits)).click();
		Assert.assertEquals(true, summitsObject.beyorbosstitle.isDisplayed(),"Title is not displayed");
		Assert.assertEquals(true, summitsObject.speakersbtn.isDisplayed(),"Speakers icon is not displayed");
		Assert.assertEquals(true, summitsObject.summittopicsbtn.isDisplayed(),"Summit Topics icon is not displayed");
		summitsObject.speakersbtn.click();
		Thread.sleep(3000);
		driver.navigate().back();
		summitsObject.summittopicsbtn.click();
		Thread.sleep(3000);
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		
	}

	@Test
	public void BEYOURBOSS_TC_004() throws InterruptedException, IOException {
		try
		{
			homeObject.exploremsg.isDisplayed();
			wait.until(ExpectedConditions.elementToBeClickable(homeObject.beyourboss)).click();

		}
		catch(Exception e)
		{
			System.out.println("Be your own Boss Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(byobObject.tvshow)).click();
		
		tvshowObject.moredetails.click();
	
		Thread.sleep(4000);
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
	}
}
