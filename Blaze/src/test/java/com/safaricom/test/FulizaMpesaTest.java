package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.commonpage.MpesaPinFinalPage;
import com.safaricom.commonpage.MpesaPinPage;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.mpesa.FulizaHomePage;
import com.safaricom.mpesa.MpesaHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class FulizaMpesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MpesaHomePage mpesahomeObject=new MpesaHomePage(driver);
	public MpesaPinPage mpesapinObject=new MpesaPinPage(driver);
	public MpesaPinFinalPage mpesapinfinalObject=new MpesaPinFinalPage(driver);
	public FulizaHomePage fulizahomeObject=new FulizaHomePage(driver);
	
	@Test
	public void BLAZEFULIZAMPESA_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			homeObject.blazempesa.click();			
			
		}
		catch(Exception e)
		{
			System.out.println("Mpesa Page");
		}
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomeObject.fulizampesaClick)).isDisplayed();	
		mpesahomeObject.fulizampesaClick.click();
		String expectedtitle = "Fuliza M-PESA";
		wait.until(ExpectedConditions.elementToBeClickable(fulizahomeObject.fuliza_title)).isDisplayed();

		String fuliza_title = fulizahomeObject.fuliza_title.getText();
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"OPT IN\"))");
		wait.until(ExpectedConditions.elementToBeClickable(fulizahomeObject.fulizacheckbox_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fulizahomeObject.fulizaoptin_continue)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(fulizahomeObject.msg)).isDisplayed();
		String expected="Your request has been received and is being processed";
		String actual=fulizahomeObject.msg.getText();
		wait.until(ExpectedConditions.elementToBeClickable(fulizahomeObject.fuliza_optin_ok)).click();
		Assert.assertEquals(fuliza_title, expectedtitle);

		Assert.assertEquals(actual, expected);
	}
	
	
}
