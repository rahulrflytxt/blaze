package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.home.HomePage;
import com.safaricom.menu.MenuItemsPage;
import com.safaricom.services.ServicesHomePage;
import com.safaricom.services.ServicesOkoaPage;
import com.safaricom.services.ServicesSkizaPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicesSkizaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public HomePage homeObject=new HomePage(driver);
	public MenuItemsPage menuObject=new MenuItemsPage(driver);
	public ServicesHomePage servicesObject=new ServicesHomePage(driver);
	public ServicesSkizaPage skizaObject=new ServicesSkizaPage(driver);

	@Test
	public void SERVICES_SKIZASERVICES_TC_001() throws InterruptedException, IOException {
		try
		{
			Thread.sleep(4000);
			homeObject.exploremsg.isDisplayed();
			
		}
		catch(Exception e)
		{
			driver.navigate().back();		
		}
		menuObject.menu_click.click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"Services\"))");
		service.click();
		Thread.sleep(2500);	
		servicesObject.skiza_click.click();
		Thread.sleep(2000);
		String expectedtitledata="Skiza Services";
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.skiza_title)).isDisplayed();

		String actual_titledata = skizaObject.skiza_title.getText();
		Assert.assertEquals(actual_titledata, expectedtitledata);
		
		Assert.assertEquals(true, skizaObject.rbartist_click.isDisplayed(), "Artist Radio button is not displayed");
		Assert.assertEquals(true, skizaObject.rbsong_click.isDisplayed(), "Song Radio button is not displayed");
		Assert.assertEquals(true, skizaObject.artist_song.isDisplayed(), "Artist/Song field is not displayed");
		Assert.assertEquals(true, skizaObject.search_btn_Click.isDisplayed(), "Search button is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.rbartist_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.artist_song)).sendKeys("A R RAHMAN");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.search_btn_Click)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.subscribe)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.tv_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaObject.tv_ok)).click();
		driver.navigate().back();
		Thread.sleep(2000);
		driver.navigate().back();
		
		
	}
}
