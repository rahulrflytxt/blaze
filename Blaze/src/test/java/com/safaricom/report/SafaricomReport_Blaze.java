package com.safaricom.report;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.safaricom.config.TestConfig;
import com.safaricom.test.BYOBTest;
import com.safaricom.test.BillmanagerTest;
import com.safaricom.test.BlazeBongaTest;
import com.safaricom.test.BlazeBundlesTest;
import com.safaricom.test.BlazeDataUsageTest;
import com.safaricom.test.BlazeDeviceTest;
import com.safaricom.test.BlazeHomeTest;
import com.safaricom.test.BlazeMpesaTest;
import com.safaricom.test.BuyAirtimeTest;
import com.safaricom.test.ServicesCheckbalanceTest;
import com.safaricom.test.ServicesContactusTest;
import com.safaricom.test.ServicesDataCallingTest;
import com.safaricom.test.FulizaMpesaTest;
import com.safaricom.test.LipanaMpesaTest;
import com.safaricom.test.LoansandsavingsTest;
import com.safaricom.test.LoginTest;
import com.safaricom.test.MenuTest;
import com.safaricom.test.MpesaglobalTest;
import com.safaricom.test.MyAccountTest;
import com.safaricom.test.SendMoneyTest;
import com.safaricom.test.ServicesAirtimeTopupTest;
import com.safaricom.test.ServicesBongaservicesTest;
import com.safaricom.test.ServicesHomeTest;
import com.safaricom.test.ServicesOkoaTest;
import com.safaricom.test.ServicesSambazaTest;
import com.safaricom.test.ServicesSkizaTest;
import com.safaricom.test.ServicesStorelocatorTest;
import com.safaricom.test.TalktousTest;
import com.safaricom.test.WithdrawCashTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SafaricomReport_Blaze {
	ExtentReports extentReport;
	ExtentTest extentTest;
	int i=0;
	LoginTest logintestestObject = new LoginTest();
	BlazeHomeTest hometestObject=new BlazeHomeTest();
	BYOBTest byobObject=new BYOBTest();
	BlazeBundlesTest blazebundleObject=new BlazeBundlesTest();
	BlazeDataUsageTest blazedatausageObject=new BlazeDataUsageTest();
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = (AppiumDriver<MobileElement>) TestConfig.getInstance().getDriver();
	BlazeBongaTest blazebongaObject=new BlazeBongaTest();
	BlazeMpesaTest blazempesaObject=new BlazeMpesaTest();
	SendMoneyTest sendmoneyObject=new SendMoneyTest();
	WithdrawCashTest withdrawcashObject=new WithdrawCashTest();
	BuyAirtimeTest buyAirtimeObject=new BuyAirtimeTest();
	LipanaMpesaTest lipanaMpesaObject=new LipanaMpesaTest();
	BillmanagerTest billmanagerObject=new BillmanagerTest();
	LoansandsavingsTest loansandsavingsObject=new LoansandsavingsTest();
	MyAccountTest myaccountObject=new MyAccountTest();
	FulizaMpesaTest fulizaObject=new FulizaMpesaTest();
	MpesaglobalTest mpesaglobalObject=new MpesaglobalTest();
	BlazeDeviceTest blazedeviceObject=new BlazeDeviceTest();
	TalktousTest talktousObject=new TalktousTest();
	ServicesHomeTest servicesObject=new ServicesHomeTest();
	ServicesCheckbalanceTest servicescheckbalanceObject=new ServicesCheckbalanceTest();
	ServicesBongaservicesTest servicesbongaObject=new ServicesBongaservicesTest();
	ServicesAirtimeTopupTest servicesairtimeObject = new ServicesAirtimeTopupTest();
	ServicesDataCallingTest servicesdatacallObject = new ServicesDataCallingTest();
	ServicesOkoaTest servicesokoaObject=new ServicesOkoaTest();
	ServicesSkizaTest servicesskizaObject=new ServicesSkizaTest();
	ServicesSambazaTest servicessambazaObject=new ServicesSambazaTest();
	ServicesStorelocatorTest servicesstoreObject=new ServicesStorelocatorTest();
	ServicesContactusTest servicescontactusObject=new ServicesContactusTest();
	MenuTest menuObject=new MenuTest();
	
	@BeforeSuite
	public void beforeSuite() {
		extentReport = new ExtentReports(System.getProperty("user.dir") + "/Blaze_Report.html", true);
		extentReport.addSystemInfo("Host Name", "Safaricom").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Rahul R");
		extentReport.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
	}

	@BeforeClass
	public void beforeClass() {
		// WebDriverWait wait = TestConfig.getInstance().getWait();
		// AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// In before method we are collecting the current running test case name
		String className = this.getClass().getSimpleName();
		extentTest = extentReport.startTest(className + "-" + method.getName());
	}

	@Test//(priority = 1)
	public void LOGIN_TC_001() throws InterruptedException, IOException {

		logintestestObject.LOGIN_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 2)
	public void BLAZEHOME_TC_001() throws InterruptedException, IOException {

		hometestObject.BLAZEHOME_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 3)
	public void BEYOURBOSS_TC_001() throws InterruptedException, IOException {

		byobObject.BEYOURBOSS_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 4)
	public void BEYOURBOSS_TC_002() throws InterruptedException, IOException {

		byobObject.BEYOURBOSS_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 5)
	public void BEYOURBOSS_TC_003() throws InterruptedException, IOException {

		byobObject.BEYOURBOSS_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 6)
	public void BEYOURBOSS_TC_004() throws InterruptedException, IOException {

		byobObject.BEYOURBOSS_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 7)
	public void BLAZEBUNDLES_TC_001() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 8)
	public void BLAZEBUNDLES_TC_002() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 9)
	public void BLAZEBUNDLES_TC_003() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 10)
	public void BLAZEBUNDLES_TC_004() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 11)
	public void BLAZEBUNDLES_TC_005() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 12)
	public void BLAZEBUNDLES_TC_006() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_006();
		Assert.assertTrue(true);
	}
	@Test//(priority = 13)
	public void BLAZEBUNDLES_TC_007() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_007();
		Assert.assertTrue(true);
	}
	@Test//(priority = 14)
	public void BLAZEBUNDLES_TC_008() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_008();
		Assert.assertTrue(true);
	}
	@Test//(priority = 15)
	public void BLAZEBUNDLES_TC_009() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_009();
		Assert.assertTrue(true);
	}
	@Test//(priority = 16)
	public void BLAZEBUNDLES_TC_010() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_010();
		Assert.assertTrue(true);
	}
	@Test//(priority = 17)
	public void BLAZEBUNDLES_TC_011() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_011();
		Assert.assertTrue(true);
	}
	@Test//(priority = 18)
	public void BLAZEBUNDLES_TC_012() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_012();
		Assert.assertTrue(true);
	}
	@Test//(priority = 19)
	public void BLAZEBUNDLES_TC_013() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_013();
		Assert.assertTrue(true);
	}
	@Test//(priority = 20)
	public void BLAZEBUNDLES_TC_014() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_014();
		Assert.assertTrue(true);
	}
	@Test//(priority = 21)
	public void BLAZEBUNDLES_TC_015() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_015();
		Assert.assertTrue(true);
	}
	@Test//(priority = 22)
	public void BLAZEBUNDLES_TC_016() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_016();
		Assert.assertTrue(true);
	}
	@Test//(priority = 23)
	public void BLAZEBUNDLES_TC_017() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_017();
		Assert.assertTrue(true);
	}
	@Test//(priority = 24)
	public void BLAZEBUNDLES_TC_018() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_018();
		Assert.assertTrue(true);
	}
	@Test//(priority = 25)
	public void BLAZEBUNDLES_TC_019() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_019();
		Assert.assertTrue(true);
	}
	@Test//(priority = 26)
	public void BLAZEBUNDLES_TC_020() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_020();
		Assert.assertTrue(true);
	}
	@Test//(priority = 27)
	public void BLAZEBUNDLES_TC_021() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_021();
		Assert.assertTrue(true);
	}
	@Test//(priority = 28)
	public void BLAZEBUNDLES_TC_022() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_022();
		Assert.assertTrue(true);
	}
	@Test//(priority = 29)
	public void BLAZEBUNDLES_TC_023() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_023();
		Assert.assertTrue(true);
	}
	@Test//(priority = 30)
	public void BLAZEBUNDLES_TC_024() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_024();
		Assert.assertTrue(true);
	}
	@Test//(priority = 31)
	public void BLAZEBUNDLES_TC_025() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_025();
		Assert.assertTrue(true);
	}
	@Test//(priority = 32)
	public void BLAZEBUNDLES_TC_026() throws InterruptedException, IOException {

		blazebundleObject.BLAZEBUNDLES_TC_026();
		Assert.assertTrue(true);
	}
	@Test//(priority = 33)
	public void BLAZEDATAUSAGE_TC_001() throws InterruptedException, IOException {

		blazedatausageObject.BLAZEDATAUSAGE_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 34)
	public void BLAZEBONGA_TC_001() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 35)
	public void BLAZEBONGA_TC_002() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 36)
	public void BLAZEBONGA_TC_003() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 37)
	public void BLAZEBONGA_TC_004() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 38)
	public void BLAZEBONGA_TC_005() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 39)
	public void BLAZEBONGA_TC_006() throws InterruptedException, IOException {

		blazebongaObject.BLAZEBONGA_TC_006();
		Assert.assertTrue(true);
	}
	
	@Test//(priority = 40)
	public void BLAZEMPESA_TC_001() throws InterruptedException, IOException {

		blazempesaObject.BLAZEMPESA_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 41)
	public void BLAZESENDMONEY_TC_001() throws InterruptedException, IOException {

		sendmoneyObject.BLAZESENDMONEY_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 42)
	public void BLAZESENDMONEY_TC_002() throws InterruptedException, IOException {

		sendmoneyObject.BLAZESENDMONEY_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 43)
	public void BLAZESENDMONEY_TC_003() throws InterruptedException, IOException {

		sendmoneyObject.BLAZESENDMONEY_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 44)
	public void BLAZEWITHDRAWCASH_TC_001() throws InterruptedException, IOException {

		withdrawcashObject.BLAZEWITHDRAWCASH_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 45)
	public void BLAZEWITHDRAWCASH_TC_002() throws InterruptedException, IOException {

		withdrawcashObject.BLAZEWITHDRAWCASH_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 46)
	public void BLAZEWITHDRAWCASH_TC_003() throws InterruptedException, IOException {

		withdrawcashObject.BLAZEWITHDRAWCASH_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 47)
	public void BLAZEBUYAIRTIME_TC_001() throws InterruptedException, IOException {

		buyAirtimeObject.BLAZEBUYAIRTIME_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 48)
	public void BLAZEBUYAIRTIME_TC_002() throws InterruptedException, IOException {

		buyAirtimeObject.BLAZEBUYAIRTIME_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEBUYAIRTIME_TC_003() throws InterruptedException, IOException {

		buyAirtimeObject.BLAZEBUYAIRTIME_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 47)
	public void LIPANAMPESA_TC_001() throws InterruptedException, IOException {

		lipanaMpesaObject.LIPANAMPESA_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 48)
	public void LIPANAMPESA_TC_002() throws InterruptedException, IOException {

		lipanaMpesaObject.LIPANAMPESA_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void LIPANAMPESA_TC_003() throws InterruptedException, IOException {

		lipanaMpesaObject.LIPANAMPESA_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 48)
	public void BLAZEBILLMANAGER_TC_001() throws InterruptedException, IOException {

		billmanagerObject.BLAZEBILLMANAGER_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEBILLMANAGER_TC_002() throws InterruptedException, IOException {

		billmanagerObject.BLAZEBILLMANAGER_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_001() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_002() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_003() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_004() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_005() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_006() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_006();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_007() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_007();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_008() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_008();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_009() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_009();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_010() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_010();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_011() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_011();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_012() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_012();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_013() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_013();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZELOANSANDSAVINGS_TC_014() throws InterruptedException, IOException {

		loansandsavingsObject.BLAZELOANSANDSAVINGS_TC_014();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMYACCOUNT_TC_001() throws InterruptedException, IOException {

		myaccountObject.BLAZEMYACCOUNT_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMYACCOUNT_TC_002() throws InterruptedException, IOException {

		myaccountObject.BLAZEMYACCOUNT_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEFULIZAMPESA_TC_001() throws InterruptedException, IOException {

		fulizaObject.BLAZEFULIZAMPESA_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_002() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_003() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_004() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_005() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_006() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_006();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_007() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_007();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_008() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_008();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_009() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_009();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_010() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_010();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_011() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_011();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEMPESAGLOBAL_TC_012() throws InterruptedException, IOException {

		mpesaglobalObject.BLAZEMPESAGLOBAL_TC_012();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZEDEVICES_TC_001() throws InterruptedException, IOException {

		blazedeviceObject.BLAZEDEVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZETALKTOUS_TC_001() throws InterruptedException, IOException {

		talktousObject.BLAZETALKTOUS_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZETALKTOUS_TC_002() throws InterruptedException, IOException {

		talktousObject.BLAZETALKTOUS_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZETALKTOUS_TC_003() throws InterruptedException, IOException {

		talktousObject.BLAZETALKTOUS_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void BLAZETALKTOUS_TC_004() throws InterruptedException, IOException {

		talktousObject.BLAZETALKTOUS_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_TC_001() throws InterruptedException, IOException {

		servicesObject.SERVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CHECKBALANCE_TC_001() throws InterruptedException, IOException {

		servicescheckbalanceObject.SERVICES_CHECKBALANCE_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CHECKBALANCE_TC_002() throws InterruptedException, IOException {

		servicescheckbalanceObject.SERVICES_CHECKBALANCE_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CHECKBALANCE_TC_003() throws InterruptedException, IOException {

		servicescheckbalanceObject.SERVICES_CHECKBALANCE_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CHECKBALANCE_TC_004() throws InterruptedException, IOException {

		servicescheckbalanceObject.SERVICES_CHECKBALANCE_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CHECKBALANCE_TC_005() throws InterruptedException, IOException {

		servicescheckbalanceObject.SERVICES_CHECKBALANCE_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_BLAZEBONGA_TC_001() throws InterruptedException, IOException {

		servicesbongaObject.SERVICES_BLAZEBONGA_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_BLAZEBONGA_TC_002() throws InterruptedException, IOException {

		servicesbongaObject.SERVICES_BLAZEBONGA_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_AIRTIMETOPUP_TC_001() throws InterruptedException, IOException {

		servicesairtimeObject.SERVICES_AIRTIMETOPUP_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_DATACALLPLANSSERVICES_TC_001() throws InterruptedException, IOException {

		servicesdatacallObject.SERVICES_DATACALLPLANSSERVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_OKOASERVICES_TC_001() throws InterruptedException, IOException {

		servicesokoaObject.SERVICES_OKOASERVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_SKIZASERVICES_TC_001() throws InterruptedException, IOException {

		servicesskizaObject.SERVICES_SKIZASERVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_SAMBAZASERVICES_TC_001() throws InterruptedException, IOException {

		servicessambazaObject.SERVICES_SAMBAZASERVICES_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_SAMBAZASERVICES_TC_002() throws InterruptedException, IOException {

		servicessambazaObject.SERVICES_SAMBAZASERVICES_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_SAMBAZASERVICES_TC_003() throws InterruptedException, IOException {

		servicessambazaObject.SERVICES_SAMBAZASERVICES_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_STORELOCATOR_TC_001() throws InterruptedException, IOException {

		servicesstoreObject.SERVICES_STORELOCATOR_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void SERVICES_CONTACTUS_TC_001() throws InterruptedException, IOException {

		servicescontactusObject.SERVICES_CONTACTUS_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_001() throws InterruptedException, IOException {

		menuObject.MENU_TC_001();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_002() throws InterruptedException, IOException {

		menuObject.MENU_TC_002();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_003() throws InterruptedException, IOException {

		menuObject.MENU_TC_003();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_004() throws InterruptedException, IOException {

		menuObject.MENU_TC_004();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_005() throws InterruptedException, IOException {

		menuObject.MENU_TC_005();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_006() throws InterruptedException, IOException {

		menuObject.MENU_TC_006();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_007() throws InterruptedException, IOException {

		menuObject.MENU_TC_007();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_008() throws InterruptedException, IOException {

		menuObject.MENU_TC_008();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_009() throws InterruptedException, IOException {

		menuObject.MENU_TC_009();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_010() throws InterruptedException, IOException {

		menuObject.MENU_TC_010();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_011() throws InterruptedException, IOException {

		menuObject.MENU_TC_011();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_012() throws InterruptedException, IOException {

		menuObject.MENU_TC_012();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_013() throws InterruptedException, IOException {

		menuObject.MENU_TC_013();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_014() throws InterruptedException, IOException {

		menuObject.MENU_TC_014();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_015() throws InterruptedException, IOException {

		menuObject.MENU_TC_015();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_016() throws InterruptedException, IOException {

		menuObject.MENU_TC_016();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_017() throws InterruptedException, IOException {

		menuObject.MENU_TC_017();
		Assert.assertTrue(true);
	}
	@Test//(priority = 49)
	public void MENU_TC_018() throws InterruptedException, IOException {

		menuObject.MENU_TC_018();
		Assert.assertTrue(true);
	}
	@AfterMethod
	public void getResult(ITestResult result, Method method) throws Exception {
		// In after method we are collecting the test execution status and based on that
		// the information writing to HTML report
		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "Test Case Failed is : " + result.getName());
			// String screenshotPath = SafaricomReport.capture(driver, result.getName());
			
			File file  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(file, new File("F:\\Rahul Automation\\Blaze Automation\\Screenshot\\Screenshot - "+ result.getName()+".jpg"));
			i++;
			extentTest.log(LogStatus.FAIL, "Error Details :- " + result.getThrowable().getMessage());// +extentTest.addScreenCapture(screenshotPath));
			driver.closeApp();
			driver.launchApp();
		}
		if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case Skipped is : " + result.getName());
		}
		if (result.getStatus() == ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case Passed is : " + result.getName());
		}
	}

	@AfterSuite
	public void endReport() {
		extentReport.endTest(extentTest);
		extentReport.flush();
		driver.quit();
	}

	
//	public static String capture(AppiumDriver<MobileElement> driver, String screenShotName) throws IOException {
//		String dest = null;
//		try {
//			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
//			Date date = new Date();
//			TakesScreenshot ts = (TakesScreenshot) driver;
//			File source = ts.getScreenshotAs(OutputType.FILE);
//			dest = System.getProperty("user.dir") + "/ErrorScreenshots/" + screenShotName + dateFormat.format(date)
//					+ ".png";
//			File destination = new File(dest);
//			FileUtils.copyFile(source, destination);
//		} catch (Exception e) {
//			e.getMessage();
//			System.out.println(e.getMessage());
//		}
//		return dest;
//	}

	
}
