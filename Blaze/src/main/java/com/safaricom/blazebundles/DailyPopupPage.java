package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DailyPopupPage {
	public AndroidDriver<MobileElement> driver;
	public DailyPopupPage() {
		
	}
	
	public DailyPopupPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//FirstPopup
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
	public AndroidElement buyoncebtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement autorenewbtn;
	
	//SecondPopup
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/title")
	public AndroidElement confirmbuytitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/message")
	public AndroidElement confirmmsg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement acceptbtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
	public AndroidElement declinebtn;
	
	//SuccesPage
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
	public AndroidElement successmsg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
	public AndroidElement successokbtn;
	
	

	
}
