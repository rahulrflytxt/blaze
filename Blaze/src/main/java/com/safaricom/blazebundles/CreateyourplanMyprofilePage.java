package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CreateyourplanMyprofilePage {
	public AndroidDriver<MobileElement> driver;
	public CreateyourplanMyprofilePage() {
		
	}
	
	public CreateyourplanMyprofilePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MY PROFILE']")
	public AndroidElement myprofile;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MY PLANS']")
	public AndroidElement myplans;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='FAVORITE PLANS']")
	public AndroidElement favouriteplans;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='AIRTIME']")
	public AndroidElement airtime;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/name")
	public AndroidElement name;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/airtime_balance")
	public AndroidElement airtimebalance;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/bundle_balance")
	public AndroidElement bundlebalance;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_daily")
	public AndroidElement dailybtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_weekly")
	public AndroidElement weeklybtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_monthly")
	public AndroidElement monthlybtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/text")
	public AndroidElement drpdown;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/data_details")
	public AndroidElement dataoffer;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/call_details")
	public AndroidElement calloffer;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/message_details")
	public AndroidElement smsoffer;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/ignite")
	public AndroidElement ignitebtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/okoa")
	public AndroidElement okoacheckbox;
	
	// Ignite button Click
	
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
		public AndroidElement confirmmsg;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
		public AndroidElement okbtn;
	
}
