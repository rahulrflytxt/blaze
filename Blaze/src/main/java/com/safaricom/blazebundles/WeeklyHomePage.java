package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WeeklyHomePage {
	public AndroidDriver<MobileElement> driver;
	public WeeklyHomePage() {
		
	}
	
	public WeeklyHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement weeklytitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement subtitle;
	
	// First Popup
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	public AndroidElement confirmmsg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement okbtn;
	
	
	
}
