package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CreateyourplanAirtimePage {
	public AndroidDriver<MobileElement> driver;
	public CreateyourplanAirtimePage() {
		
	}
	
	public CreateyourplanAirtimePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/my_airtume_balance")
	public AndroidElement myaitimebalancesubtitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/date")
	public AndroidElement date;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_airtime_balance")
	public AndroidElement balance;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/topup")
	public AndroidElement topuptext;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/date1")
	public AndroidElement date2;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/enter_pin_text")
	public AndroidElement enter16text;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	public AndroidElement textfield16;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/borrow_airtime")
	public AndroidElement okoajahazi;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/load_airtime")
	public AndroidElement loadairtime;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buy_with_mpesa")
	public AndroidElement buyairtimempesa;
	
	
	//Confirmation
	
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
		public AndroidElement confirmmsg;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
		public AndroidElement okbtn;
	
}
