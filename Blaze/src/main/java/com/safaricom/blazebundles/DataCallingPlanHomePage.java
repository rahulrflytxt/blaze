package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DataCallingPlanHomePage {
	public AndroidDriver<MobileElement> driver;
	public DataCallingPlanHomePage() {
		
	}
	
	public DataCallingPlanHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement datacallingtitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/airtimeBalanceTV")
	  public AndroidElement airtimeBalance ;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/edtValue")
	  public AndroidElement amountField;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/dataBundlesWithoutExpiryTV")
	  public AndroidElement dataBundlesWithoutExpiry;
}
