package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CreateyourplanFavoritePage {
	public AndroidDriver<MobileElement> driver;
	public CreateyourplanFavoritePage() {
		
	}
	
	public CreateyourplanFavoritePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='DAILY']")
	public AndroidElement dailydrop;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/textViewDataValue")
	public AndroidElement data;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/textViewCallValue")
	public AndroidElement call;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/textViewSMSValue")
	public AndroidElement sms;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/textViewTotalSpendValue")
	public AndroidElement totalspend;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonApply")
	public AndroidElement applybtn;
	
	//Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	public AndroidElement confirmmsg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement okbtn;
}
