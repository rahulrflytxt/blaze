package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BlazeBundleHomePage {
	public AndroidDriver<MobileElement> driver;
	public BlazeBundleHomePage() {
		
	}
	
	public BlazeBundleHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement blazebundletitle;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='DATA & CALLING PLANS']")
	public AndroidElement datandcallingplans;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='DAILY']")
	public AndroidElement daily;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='WEEKLY']")
	public AndroidElement weekly;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MONTHLY']")
	public AndroidElement monthly;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='POWER HOUR BUNDLES']")
	public AndroidElement powerhour;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='CREATE YOUR PLAN']")
	public AndroidElement createyourplan;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='CHECK BALANCE']")
	public AndroidElement checkbalance;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MY PLAN HISTORY']")
	public AndroidElement myplanhistory;
}
