package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyplanhistoryHomePage {

	  public AndroidDriver<MobileElement> driver;
	  
	  public MyplanhistoryHomePage() {
		  
	  }
	  public MyplanhistoryHomePage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/containerFromDate")
	  public AndroidElement fromdate;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/containerToDate")
	  public AndroidElement todate;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonGenerate")
	  public AndroidElement generatebtn;
	  
	  // DatePicker
	  
	  @AndroidFindBy(id="android:id/prev")
	  public AndroidElement prevbtn;
	  
	  @AndroidFindBy(id="//android.view.View[@content-desc=\"01 November 2019\"]")
	  public AndroidElement datenov1;
	  
	  @AndroidFindBy(id="android:id/button1")
	  public AndroidElement okbtn;
	  
	  //Confirmation
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
		public AndroidElement okbtn1;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
		public AndroidElement msg;
		
	  
	  
	

}
