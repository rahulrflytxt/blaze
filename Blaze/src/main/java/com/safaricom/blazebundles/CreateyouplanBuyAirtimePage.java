package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CreateyouplanBuyAirtimePage {
	public AndroidDriver<MobileElement> driver;
	public CreateyouplanBuyAirtimePage() {
		
	}
	
	public CreateyouplanBuyAirtimePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_own")
	public AndroidElement mynum;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_other")
	public AndroidElement othernum;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	public AndroidElement amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
	public AndroidElement continuebtn;
	
	// Other Number
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement number;
	
	
	//Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	public AndroidElement confirmtitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	public AndroidElement confirmname;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	public AndroidElement confirmamount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	public AndroidElement confirmtranscost;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	public AndroidElement sendbtn;
	
	
	
	
}
