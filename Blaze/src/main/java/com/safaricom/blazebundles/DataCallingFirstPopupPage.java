package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DataCallingFirstPopupPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public DataCallingFirstPopupPage() {
		  
	  }
	  public DataCallingFirstPopupPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement confirmation_message;
	    
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
	  public AndroidElement tv_cancel;
	    
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement tv_ok ;
	  
	  //No Expiry
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_own")
	  public AndroidElement ownnumber ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_other")
	  public AndroidElement othernumber ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_airtime")
	  public AndroidElement airtime;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_mpesa")
	  public AndroidElement mpesa;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_msisdn")
	  public AndroidElement othernumbertext ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/continueclick")
	  public AndroidElement continuebutton ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/cancel")
	  public AndroidElement cancelbutton ;
	 
}
