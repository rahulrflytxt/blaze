package com.safaricom.blazebundles;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DataCallingFinalConfirmPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public DataCallingFinalConfirmPage() {
		  
	  }
	  public DataCallingFinalConfirmPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement finalconfirmation_message1;

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement tv_ok1 ;

	  
	//No Expiry
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement confirmheader ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/cancel")
	  public AndroidElement cancelbtn ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/continueclick")
	  public AndroidElement buybtn;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/message_text")
	  public AndroidElement confirmtxt;
	  
	  //FinalConfirm
	  @AndroidFindBy(id="com.selfcare.safaricom:id/desc")
	  public AndroidElement finalconfirmtext ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/ok")
	  public AndroidElement okbtn ;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	  public AndroidElement finalmpesaconfirmtext;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement closebtn;
}
