package com.safaricom.login;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class GeneratePinPage {
	public AndroidDriver<MobileElement> driver;

	public GeneratePinPage() {

	}

	public GeneratePinPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.selfcare.safaricom:id/et_autofill_phn")
	public MobileElement accountNumber;

	@AndroidFindBy(id = "com.selfcare.safaricom:id/tv_generate_pin")
	public MobileElement submitButton;
}
