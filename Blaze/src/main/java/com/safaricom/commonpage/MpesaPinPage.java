package com.safaricom.commonpage;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MpesaPinPage {
	public AndroidDriver<MobileElement> driver;
	public MpesaPinPage()
	{
		
	}
	
	public MpesaPinPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement sendmoney_title ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_1")
	  public AndroidElement one_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_2")
	  public AndroidElement two_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_3")
	  public AndroidElement three_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_4")
	  public AndroidElement four_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_5")
	  public AndroidElement five_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_6")
	  public AndroidElement six_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_7")
	  public AndroidElement seven_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_8")
	  public AndroidElement eight_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_9")
	  public AndroidElement nine_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_0")
	  public AndroidElement zero_btn ;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_ok")
	  public AndroidElement ok_btn ;
	 
}
