package com.safaricom.blazebonga;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TransferbongapointsPage {
	public AndroidDriver<MobileElement> driver;
	public TransferbongapointsPage() {
		
	}
	
	public TransferbongapointsPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_title_bonga_points")
	public AndroidElement subtitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_bonga_mobile_num")
	public AndroidElement numberfield;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_num_of_points")
	public AndroidElement pointsfield;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_points_transfer")
	public AndroidElement trasferbtn;
	
	//Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement okbtn1;
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	public AndroidElement msg;
	
	//ServicePinPage
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/text_heading")
	public AndroidElement servicetitle;
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	public AndroidElement pinfield;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
	public AndroidElement serviceokbtn;
		
	
	
	
}
