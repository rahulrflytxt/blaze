package com.safaricom.blazebonga;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BlazebongaHomePage {
	public AndroidDriver<MobileElement> driver;
	public BlazebongaHomePage() {
		
	}
	
	public BlazebongaHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;

//	@AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE BONGA']")
//	public AndroidElement blazebonga;


	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='SEE REDEMPTION HISTORY']")
	public AndroidElement seeredemptionhistory;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='SEE ACCUMULATION HISTORY']")
	public AndroidElement seeaccumulationhistory;

}
