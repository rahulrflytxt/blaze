package com.safaricom.blazebonga;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BlazeBongaPage {
	public AndroidDriver<MobileElement> driver;
	public BlazeBongaPage() {
		
	}
	
	public BlazeBongaPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement title;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MY BLAZE BONGA']")
	public AndroidElement myblazebongatab;
		
	@AndroidFindBy(xpath="//android.widget.TextView[@text='BONGA OFFERS']")
	public AndroidElement bongaofferstab;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_bonga_balance")
	public AndroidElement curbalance;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_bonga_date")
	public AndroidElement date;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_transfer")
	public AndroidElement transferbtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem")
	public AndroidElement redeembtn;
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement certificateokbtn;
}
