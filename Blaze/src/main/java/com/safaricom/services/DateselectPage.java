package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DateselectPage {
	 public AndroidDriver<MobileElement> driver;
	  
	  public DateselectPage() {
		  
	  }
	  public DateselectPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }

	  @AndroidFindBy(id="android:id/date_picker_header_year")
	  public AndroidElement date_picker_header_year;
	  
	  @AndroidFindBy(id="android:id/date_picker_header_date")
	  public AndroidElement date_picker_header_date;
	    
	  @AndroidFindBy(xpath="//android.widget.Button[@text='OK']")
	  public AndroidElement ok_button ;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement ok ;
	  
	  

	    
	  @AndroidFindBy(xpath="//android.widget.Button[@text='Cancel']")
	  public AndroidElement cancel_button ;
}
