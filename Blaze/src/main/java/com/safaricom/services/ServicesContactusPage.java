package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesContactusPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public ServicesContactusPage() {
		  
	  }
	  public ServicesContactusPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  // ContactUs Home

	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement contactUs_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/twitter")
	  public AndroidElement img_twitter_click;
	 
	  @AndroidFindBy(id="com.selfcare.safaricom:id/facebook")
	  public AndroidElement img_facebook_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/instagram")
	  public AndroidElement img_insta_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_youtube")
	  public AndroidElement txt_youtube_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_blog")
	  public AndroidElement txt_blog_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_google_plus")
	  public AndroidElement txt_google_plus_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/new_request")
	  public AndroidElement new_request_click ;
}
