package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesRedeembongaPage {
	public AndroidDriver<MobileElement> driver;
	public ServicesRedeembongaPage() {
		
	}
	
	public ServicesRedeembongaPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_redeem_option_field")
	  public AndroidElement redeem_option;
	    
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Minutes']")
	  public AndroidElement redeem_option_select ;
	    


	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_select_amount_field")
	  public AndroidElement amount_field ;

	  @AndroidFindBy(xpath="//android.widget.TextView[@text='4 min for 50 pts']")
	  public AndroidElement amount_field_select ;

	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_redeem")
	  public AndroidElement redeem_button;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_title_bonga_points")
	  public AndroidElement title_bonga_points ;
	  

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement confirmation_message;
	    
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement ok_click ;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_cancel")
	  public AndroidElement cancel_click ;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement final_confirmation_message;

}
