package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesHomePage {
	public AndroidDriver<MobileElement> driver;
	  
	  public ServicesHomePage() {
		  
	  }
	  public ServicesHomePage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement services_title;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Check Balance']")
		public AndroidElement checkbalance_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga Services']")
		public AndroidElement bongaservices_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Airtime Top Up']")
		public AndroidElement airtimetopup_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data & Calling Plans']")
		public AndroidElement datacall_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa Services']")
		public AndroidElement okoa_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Skiza Services']")
		public AndroidElement skiza_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Sambaza Services']")
		public AndroidElement sambaza_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Store Locator']")
		public AndroidElement storelocator_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Contact us']")
		public AndroidElement contactus_click;
	  
}
