package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesOkoaPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public ServicesOkoaPage() {
		  
	  }
	  public ServicesOkoaPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement okoa_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_my_account_no_data")
	  public AndroidElement nodatamsg;
}
