package com.safaricom.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesSkizaPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public ServicesSkizaPage() {
		  
	  }
	  public ServicesSkizaPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement skiza_title;
	  
	   
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_artist")
	  public AndroidElement rbartist_click;
	      
	  @AndroidFindBy(id="com.selfcare.safaricom:id/rb_Song")
	  public AndroidElement rbsong_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/entr_artist_song")
	  public AndroidElement artist_song ;

	  @AndroidFindBy(id="com.selfcare.safaricom:id/subscribe_hottest_tune")
	  public AndroidElement subscribe ;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tune_search_btn")
	  public AndroidElement search_btn_Click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement tv_message;
	      
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement tv_ok;
}
