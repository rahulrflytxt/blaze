package com.safaricom.datausage;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DatausageHomePage {
	public AndroidDriver<MobileElement> driver;
	public DatausageHomePage() {
		
	}
	
	public DatausageHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/startDate")
	  public AndroidElement fromdate;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/endDate")
	  public AndroidElement todate;
	
	// DatePicker
	  
		  @AndroidFindBy(id="android:id/prev")
		  public AndroidElement prevbtn;
		  
		  @AndroidFindBy(id="//android.view.View[@content-desc=\"01 November 2019\"]")
		  public AndroidElement datenov1;
		  
		  @AndroidFindBy(id="android:id/button1")
		  public AndroidElement okbtn;
		  
	
}
