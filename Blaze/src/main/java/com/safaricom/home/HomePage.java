package com.safaricom.home;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomePage {

	public AndroidDriver<MobileElement> driver;
	public HomePage() {
		
	}
	
	public HomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
		//Sfc
	
	  @AndroidFindBy(className="android.widget.ImageButton")
	  public AndroidElement menutab_click;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement ok_button;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
	  public AndroidElement okbutton;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	  public AndroidElement sfctitle; 
	  
	  	//Blaze
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='WELCOME']")
	  public AndroidElement welcomemsg;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Explore the BLAZE world']")
	  public AndroidElement exploremsg;
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_blaze_username")
	  public AndroidElement name;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BE YOUR OWN BOSS']")
	  public AndroidElement beyourboss;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE BUNDLES']")
	  public AndroidElement blazebundles;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE DATA USAGE']")
	  public AndroidElement blazedatausage;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE BONGA']")
	  public AndroidElement blazebonga;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE M-PESA']")
	  public AndroidElement blazempesa;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE DEVICES']")
	  public AndroidElement blazedevices;
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='TALK TO US']")
	  public AndroidElement talktous;
	  
}
