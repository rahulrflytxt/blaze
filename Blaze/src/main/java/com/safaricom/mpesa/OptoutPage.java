package com.safaricom.mpesa;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OptoutPage {
	public AndroidDriver<MobileElement> driver;

	public OptoutPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement mpesaglobal_title; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/you_can_opt_in_anytime")
	public AndroidElement optoutMsg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement confirm;

	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonContinue")
	public AndroidElement submit;
	
	@FindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement labelmsg;
	
	@FindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement lastokbtn;


}
