package com.safaricom.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CostEstimatorHomePage {
	public AndroidDriver<MobileElement> driver;

	public CostEstimatorHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement mpesaglobal_title; //
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Send Money to Mobile Number']")
	public AndroidElement sendtomobile;
		
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Bank Account']")
	public AndroidElement bankaccount;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Western Union Location']")
	public AndroidElement westernunionlocation;
	
	
	//SendMobileNumber
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelSourceOfFunds")
	public AndroidElement sendMobileSubtitle;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Select Country']")
	public AndroidElement countrydrop;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/text")
	public  List<MobileElement> L1;
	
	@FindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement amount;
	
	@FindBy(id="com.selfcare.safaricom:id/buttonContinue")
	public AndroidElement continuebtn;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/labelValue")
	public  List<MobileElement> confirmL1;
	
	@FindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement closebtn;
	

	//BankAccount
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelSourceOfFunds")
	public AndroidElement bankaccountSubtitle;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Select Currency']")
	public AndroidElement curencydrop;
	
	//WesternUnion
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelSourceOfFunds")
	public AndroidElement westernunionSubtitle;
}
