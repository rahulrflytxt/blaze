package com.safaricom.mpesa;



import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendMoneyHomePage {
	public AndroidDriver<MobileElement>  driver;
	public SendMoneyHomePage()
	{
		
	}
	
	public SendMoneyHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement SendMoney_title;
	
	
	// Send to One
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOneToggle")
	public AndroidElement SendtoOneClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOnePhoneNumber")
	public AndroidElement SendtoOnePhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOnePhoneAmount")
	public AndroidElement SendtoOnePhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOneContinue")
	public AndroidElement SendtoOneContinue;
		
	
	// Send to Other
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOtherToggle")
	public AndroidElement SendtoOtherClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherPhoneNumber")
	public AndroidElement SendtoOtherPhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherAmount")
	public AndroidElement SendtoOtherPhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOtherContinue")
	public AndroidElement SendToOtherContinue;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement errormsg_close;
	
	//Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	public AndroidElement confirmation_title;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	public AndroidElement sendto;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_mobile_value")
	public AndroidElement mobileno;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_agent_value")
	public AndroidElement amount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_amount_value")
	public AndroidElement transcost;
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	public AndroidElement sendmoney_cancel;
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	public AndroidElement sendmoney_continue;

	//Other - Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement final_confirmation_label;
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement Sendmoney_ok_click;
	
	
}
