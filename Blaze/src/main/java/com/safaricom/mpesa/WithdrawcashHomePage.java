package com.safaricom.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawcashHomePage {
	public AndroidDriver<MobileElement> driver;
	public WithdrawcashHomePage()
	{
		
	}
	
	public WithdrawcashHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement withdrawcash_title;
	
	// Withdraw From Agent
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/iv_expand_ico")
	public  List<MobileElement> l1_click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement edt_mobilenumber_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
	public AndroidElement et_amount_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/continue_sf")
	public AndroidElement WFAgentContinueClick;
			
		
	//Withdraw From ATM
	
	 	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	public AndroidElement edt_agentnumber_WFATM;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/continue_sf")
	public AndroidElement WFATMContinueClick;
	
	//Confirmation
	

	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	public AndroidElement confirmation_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_account_number")
	public AndroidElement txt_dialog_account_number;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	public AndroidElement txt_dialog_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	public AndroidElement tv_mpesa_sendto_value;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	public AndroidElement txt_continue_dilaog;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	public AndroidElement txt_cancel_dilaog;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_business_val_")
	public AndroidElement txt_business_val_atm;
	
		
}
