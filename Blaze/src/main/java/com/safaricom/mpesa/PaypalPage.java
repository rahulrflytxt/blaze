package com.safaricom.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PaypalPage {
	public AndroidDriver<MobileElement> driver;

	public PaypalPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//FirstPage
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement mpesaglobal_title; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement paypaltitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/iconPaypal")
	public AndroidElement paypalicon;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/container")
	public  List<MobileElement> L1;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelCurrency")
	public AndroidElement currency;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonContinue")
	public AndroidElement continuebtn;
	
	
	
	//ConfirmationPage
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement confrimationheader;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/labelValue")
	public  List<MobileElement> confirmL1;
	
	@FindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement labelmsg;
	
	@FindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement lastokbtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement nextbtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNegative")
	public AndroidElement canclbtn;
	
	
}
