package com.safaricom.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendtoMobilePage {
	public AndroidDriver<MobileElement> driver;

	public SendtoMobilePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//FirstPage
	@FindBy(how = How.CLASS_NAME,using="android.widget.LinearLayout")
	public  List<MobileElement> L1;
	
	//SecondPage
	@FindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement countrytitle;
	
	@FindBy(id="com.selfcare.safaricom:id/labelCountryCode")
	public AndroidElement countrycode;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/label")
	public  List<MobileElement> L2;
	
	@FindBy(id="com.selfcare.safaricom:id/inputNumber")
	public AndroidElement phonenumber;
	
	@FindBy(id="com.selfcare.safaricom:id/labelCurrency")
	public AndroidElement currency;
	
	@FindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement amount;
	
	@FindBy(id="com.selfcare.safaricom:id/buttonContinue")
	public AndroidElement continuebtn;
	
	//ConfirmationPage
	@FindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement confirmheader;
	
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/labelValue")
	public  List<MobileElement> ConfirmL1;
	
	@FindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement okbtn;
	
	@FindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement labelmsg;
	
	@FindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement lastokbtn;
	}
