package com.safaricom.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FulizaHomePage {
	public AndroidDriver<MobileElement> driver;
	public FulizaHomePage()
	{
		
	}
	
	public FulizaHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	// Fuliza Optin
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement fuliza_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/checkBoxTermsBlaze")
	public AndroidElement fulizacheckbox_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement fulizaoptin_continue;
	
	//Confirmation
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement msg;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement fuliza_optin_ok;
}
