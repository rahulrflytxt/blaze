package com.safaricom.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BillmanagerHomePage {
	public AndroidDriver<MobileElement> driver;
	public BillmanagerHomePage()
	{
		
	}
	
	public BillmanagerHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	 public AndroidElement billmanager_title;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='Popular Bills']")
	 public AndroidElement populartab;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='Frequently Paid']")
	 public AndroidElement frequenttab;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='Manage Bills']")
	 public AndroidElement managetab;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='KPLC Prepaid']")
	 public AndroidElement firstpopbill;
	 
	 //PayBill
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/labelPayBill")
	 public AndroidElement paybiltitle;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/etAccountNo")
	 public AndroidElement accnumber;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	 public AndroidElement continuebtn;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	 public AndroidElement msg;
	 
	 @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	 public AndroidElement okbtn;
	  
}
