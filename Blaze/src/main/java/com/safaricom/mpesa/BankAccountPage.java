package com.safaricom.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BankAccountPage {
	public AndroidDriver<MobileElement> driver;

	public BankAccountPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//FirstPage
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
	public AndroidElement mpesaglobal_title; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txtTitle")
	public AndroidElement subtitle_Countrydetails; 
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/text")
	public  List<MobileElement> L1;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Select Country']")
	public AndroidElement countrydrop;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Select Currency']")
	public AndroidElement curencydrop;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txtSubTitle")
	public AndroidElement subtitle_Receiverdetails; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputFirstName")
	public AndroidElement firstname; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputMiddleName")
	public AndroidElement middlename; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputLastName")
	public AndroidElement lastname; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAddress")
	public AndroidElement address; 
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement nextbtn;
	
	
	
	//SecondPage - BankDetails
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputBankName")
	public AndroidElement bankname;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSwiftCodeIBAN")
	public AndroidElement swiftcode;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAccountNumber")
	public AndroidElement accountnumber;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputReference")
	public AndroidElement reference;
	
	
	//ThirdPage
	
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/label")
	public  List<MobileElement> L2;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputPurpose")
	public AndroidElement purpose;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonContinue")
	public AndroidElement submitbtn;
	
	
	//ConfirmationPage
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement confrimationheader;
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/labelValue")
	public  List<MobileElement> confirmL1;
	
	@FindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement labelmsg;
	
	@FindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement lastokbtn;
}
