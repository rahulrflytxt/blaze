package com.safaricom.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LipanaHomePage {
	public AndroidDriver<MobileElement> driver;
	public LipanaHomePage()
	{
		
	}
	
	public LipanaHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement lipanampesa_title;
	
	//PayBill
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvPaybill")
	public AndroidElement paybillClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_buss_no")
	public AndroidElement paybill_edt_buss_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_lipa_account_number")
	public AndroidElement paybill_edt_account_number;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_bill_amount")
	public AndroidElement paybill_edt_bill_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_bill_continue")
	public AndroidElement paybill_continue;
		
	//Buy Goods
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvBuygoods")
	public AndroidElement BuygoodsClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_till_no")
	public AndroidElement Buygoods_edt_till_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_till_amount")
	public AndroidElement Buygoods_edt_till_amount;
				
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_buygoods_continue")
	public AndroidElement Buygoods_continue;
	
	//Confirmation
	
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title") //CONFIRM
		public AndroidElement lipanampesa_confirmation_title;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_payroll")
		public AndroidElement txt_dialog_payroll;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_business_val_")
		public AndroidElement txt_business_val_;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_account_number") // 
		public AndroidElement txt_dialog_account_number;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
		public AndroidElement txt_dialog_amount;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_mobile_value")
		public AndroidElement tv_mpesa_mobile_value;
			
			
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
		public AndroidElement txt_cancel_dilaog;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
		public AndroidElement txt_continue_dilaog;
}
