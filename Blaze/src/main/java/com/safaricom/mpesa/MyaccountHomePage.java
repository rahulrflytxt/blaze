package com.safaricom.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyaccountHomePage {
	 public AndroidDriver<MobileElement> driver;
	  
	  public MyaccountHomePage() {
		  
	  }
	  public MyaccountHomePage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  	public AndroidElement title;
	  
	  	@AndroidFindBy(id="com.selfcare.safaricom:id/mpesa_title")
	  	public AndroidElement mpesaUnlock;
	  
	  	//My Account Home Page
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
		public AndroidElement mpesaunlock_labelTitle; //UNLOCK YOUR M-PESA ACCOUNT
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/inputPin")
		public AndroidElement inputPin;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
		public AndroidElement buttonPositive;
		
		//My Account Confirmation
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
		public AndroidElement mpesaunlock_labelMessage; //UNLOCK YOUR M-PESA ACCOUNT
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/button")
				public AndroidElement ok_button;
		
		@AndroidFindBy(id="com.selfcare.safaricom:id/txt_failed_msg")
		public AndroidElement mpesaunlock_labelMessage1;
			
}
