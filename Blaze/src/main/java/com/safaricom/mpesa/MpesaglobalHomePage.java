package com.safaricom.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MpesaglobalHomePage {
	public AndroidDriver<MobileElement> driver;

	public MpesaglobalHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// MPESA Global 

	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement mpesaglobal_title; //
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Send Money to Mobile Number']")
	public AndroidElement sendtomobile;
	
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Bank Account']")
	public AndroidElement bankaccount;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Western Union Location']")
	public AndroidElement westernunionlocation;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Paypal']")
	public AndroidElement paypal;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='Cost Estimator']")
	public AndroidElement costestimator;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/optout")
	public AndroidElement optoutbtn;
	
	
}
