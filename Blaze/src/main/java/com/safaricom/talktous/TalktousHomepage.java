package com.safaricom.talktous;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TalktousHomepage {
	public AndroidDriver<MobileElement> driver;

	public TalktousHomepage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement talktous_title; 
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='CHAT WITH BLAZE CUSTOMER CARE AGENT']")
	public AndroidElement chatwithcustomercare;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='SHARE YOUR STORY']")
	public AndroidElement shareyourstory;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='FAQs']")
	public AndroidElement faqs;
}
