package com.safaricom.talktous;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LivechatHomepage {
	public AndroidDriver<MobileElement> driver;

	public LivechatHomepage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement livechat_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	public AndroidElement certificatemsg;

	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement certificateokbtn;
	
	@AndroidFindBy(id="eg-chat-header")
	public AndroidElement header;
	
	
	
	
	
}
