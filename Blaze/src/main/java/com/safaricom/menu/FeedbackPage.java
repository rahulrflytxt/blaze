package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FeedbackPage {
	 public AndroidDriver<MobileElement> driver;
	  
	  public FeedbackPage() {
		  
	  }
	  public FeedbackPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement feedback_title; //FEEDBACK & RATING
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/feedback_category_spinner")
	  public AndroidElement feedback_category_spinner;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='General Feedback']")
	  public AndroidElement feedback_Category_click;
	    
	  @AndroidFindBy(id="com.selfcare.safaricom:id/feedback_email")
	  public AndroidElement feedback_email;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/feedback_subject")
	  public AndroidElement feedback_subject;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/feedback_msg")
	  public AndroidElement feedback_msg;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/feedback_submit")
	  public AndroidElement feedback_submit;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement ok_btn;
}
