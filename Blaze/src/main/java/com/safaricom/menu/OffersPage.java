package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OffersPage {
	 public AndroidDriver<MobileElement> driver;
	  
	  public OffersPage() {
		  
	  }
	  public OffersPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement offers_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textTitle")
	  public AndroidElement recentsubtitle;
	  
	  
	  //Confirmation
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_confirm_msg")
	  public AndroidElement cofirmmsg;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_accept")
	  public AndroidElement acceptbtn;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_decline")
	  public AndroidElement declinebtn;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
	  public AndroidElement msg;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
	  public AndroidElement okbtn;
	  
	  
	  
}
