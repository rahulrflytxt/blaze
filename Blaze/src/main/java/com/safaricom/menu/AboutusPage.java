package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AboutusPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public AboutusPage() {
		  
	  }
	  public AboutusPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement aboutUs_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/img_app_logo")
	  public AndroidElement img_app_logo;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE app']")
	  public AndroidElement app_name;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/blaze_version")
	  public AndroidElement app_version;
	 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/blaze_terms")
	  public AndroidElement terms_and_condn;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/blaze_first_login")
	  public AndroidElement firstlogin;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/blaze_last_access")
	  public AndroidElement lastaccess;
	  

}
