package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SecurewithpinPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public SecurewithpinPage() {
		  
	  }
	  public SecurewithpinPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement confirmtxt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement confirmyesbtn;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/text_heading")
	  public AndroidElement header;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	  public AndroidElement pinfield;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
	  public AndroidElement okbtn;

	  

}
