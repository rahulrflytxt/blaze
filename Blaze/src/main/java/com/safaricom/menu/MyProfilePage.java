package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyProfilePage {
	public AndroidDriver<MobileElement> driver;
	public MyProfilePage() {
		
	}
	
	public MyProfilePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement myprofile_title;
	@AndroidFindBy(id="com.selfcare.safaricom:id/img_profile")
	  public AndroidElement profilepic;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_name")
	  public AndroidElement name;
	@AndroidFindBy(id="com.selfcare.safaricom:id/email_header")
	  public AndroidElement email;
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_edit_email")
	  public AndroidElement emailtxt;
	@AndroidFindBy(id="com.selfcare.safaricom:id/imei_header")
	  public AndroidElement imei;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_imi_number")
	  public AndroidElement imeitxt;
	@AndroidFindBy(id="com.selfcare.safaricom:id/puk_header")
	  public AndroidElement puk;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_puk_number")
	  public AndroidElement puktxt;

	
}
