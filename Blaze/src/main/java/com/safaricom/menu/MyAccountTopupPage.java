package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyAccountTopupPage {
	 public AndroidDriver<MobileElement> driver;
	  
	  public MyAccountTopupPage() {
		  
	  }
	  public MyAccountTopupPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement txt_title;
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_topup_asat")
	  public AndroidElement date;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_airtime_balance")
	  public AndroidElement balance;
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_own")
	  public AndroidElement mynumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_other")
	  public AndroidElement othernumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	  public AndroidElement numberfield;
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	  public AndroidElement digitfield;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
	  public AndroidElement sendbtn;
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_m_pesa_topup")
	  public AndroidElement mpesatopupbtn;
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem_bonga")
	  public AndroidElement redeembongabtn;
	@AndroidFindBy(id="com.selfcare.safaricom:id/lyt_background")
	  public AndroidElement contactus;
	
	//Confirmation
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement msg;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement okbtn;
	
}
