package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MenuItemsPage {
	public AndroidDriver<MobileElement> driver;
	public MenuItemsPage()
	{
		
	}
	
	public MenuItemsPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 @AndroidFindBy(xpath="//android.widget.ImageButton[@content-desc=\"Drawer Open\"]")
	 public AndroidElement menu_click;
	 
}
