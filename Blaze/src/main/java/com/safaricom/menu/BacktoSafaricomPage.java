package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BacktoSafaricomPage {
	public AndroidDriver<MobileElement> driver;
	  
	  public BacktoSafaricomPage() {
		  
	  }
	  public BacktoSafaricomPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_message")
	  public AndroidElement tc_msg;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	  public AndroidElement tc_ok;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
	  public AndroidElement success_msg;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
	  public AndroidElement success_ok;
	  
	  
}
