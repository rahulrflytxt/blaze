package com.safaricom.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyAccountPUKPage {
	  public AndroidDriver<MobileElement> driver;
	  
	  public MyAccountPUKPage() {
		  
	  }
	  public MyAccountPUKPage(AndroidDriver<MobileElement> driver)
	  {
		  this.driver=driver;
		  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
	  }
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement txt_title;
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_my_num")
	  public AndroidElement mynumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/rb_other_num")
	  public AndroidElement othernumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
	  public AndroidElement mobnumfield;
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
	  public AndroidElement idfield;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
	  public AndroidElement sendbtn;
	@AndroidFindBy(id="com.selfcare.safaricom:id/lyt_background")
	  public AndroidElement contactus;
	
	
	//My Number
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_your_puk")
	  public AndroidElement field;
	@AndroidFindBy(id="com.selfcare.safaricom:id/iv_puk_copy")
	  public AndroidElement copy;
	
	//Confirmation
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_failed_msg")
	  public AndroidElement msg;
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_failed_advice")
	  public AndroidElement okbtn;
	
	
	
	
}
