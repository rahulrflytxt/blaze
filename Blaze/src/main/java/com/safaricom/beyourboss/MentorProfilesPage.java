package com.safaricom.beyourboss;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MentorProfilesPage {
	public AndroidDriver<MobileElement> driver;
	public MentorProfilesPage() {
		
	}
	
	public MentorProfilesPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement beyorbosstitle;
	
	@AndroidFindBy(xpath="(//android.widget.ImageView[@content-desc=\"MENTOR PROFILES\"])[1]")
	public AndroidElement mentor1;
	
	@AndroidFindBy(xpath="(//android.widget.ImageView[@content-desc=\"MENTOR PROFILES\"])[2]")
	public AndroidElement mentor2;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MENTOR PROFILES']")
	public AndroidElement mentorprofilesTab;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='SUMMITS']")
	public AndroidElement summitsTab;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='TV SHOW']")
	public AndroidElement tvshowTab;
	
	
	
	
	
}
