package com.safaricom.beyourboss;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MentorProfilesSecondPage {
	public AndroidDriver<MobileElement> driver;
	public MentorProfilesSecondPage() {
		
	}
	
	public MentorProfilesSecondPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement beyorbosstitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mentor")
	public AndroidElement mentortitle;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/img_mentor")
	public AndroidElement mentorimage;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/ftv")
	public AndroidElement mentordesc;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_next")
	public AndroidElement nextbtn;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_previous")
	public AndroidElement prevbtn;
	
	
	
	
	
	
}
