package com.safaricom.beyourboss;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BeyourbossHomePage {
	public AndroidDriver<MobileElement> driver;
	public BeyourbossHomePage() {
		
	}
	
	public BeyourbossHomePage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement beyorbosstitle;
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MENTOR PROFILES']")
	public AndroidElement mentorprofiles;
	@AndroidFindBy(xpath="//android.widget.TextView[@text='SUMMITS']")
	public AndroidElement summits;
	@AndroidFindBy(xpath="//android.widget.TextView[@text='TV SHOW']")
	public AndroidElement tvshow;
	

}
