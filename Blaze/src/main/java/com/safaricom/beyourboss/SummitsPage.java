package com.safaricom.beyourboss;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SummitsPage {
	public AndroidDriver<MobileElement> driver;
	public SummitsPage() {
		
	}
	
	public SummitsPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement beyorbosstitle;
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_speakers")
	public AndroidElement speakersbtn;
	@AndroidFindBy(id="com.selfcare.safaricom:id/btn_summit_topic")
	public AndroidElement summittopicsbtn;
	
}
